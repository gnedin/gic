C
C  Low-res grid size and the high-res grid refinement level
C  (NumHigh = NumLow*2**NumRef)
C
	parameter (NumLow = 128)
	parameter (NumRef = 2)
C
C  Use these if you need a non-cubic grid
C
	parameter (NumLowX = NumLow)
	parameter (NumLowY = NumLow)
	parameter (NumLowZ = NumLow)

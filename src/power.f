      function Pk(ak)
      include 'power.inc'
      common /tfdata1/ ndata, akdata(NMDATA), alkmin, alkmax, alkstp
      common /tfdata2/ tkdata(NMDATA)
C
      alkc = log(ak+1.0e-35)
      if(alkc .lt. alkmin) then
         tk = 1.0
      else
         lpd = 1+int((alkc-alkmin)/alkstp)
         lpu = lpd + 1
         if(lpu .gt. ndata) then
            lpu = ndata
            lpd = ndata-1
         endif
         wu = (alkc-akdata(lpd))/alkstp
         wd = (akdata(lpu)-alkc)/alkstp
         tk = exp(wu*tkdata(lpu)+wd*tkdata(lpd))
      endif

      Pk = Ampl*(ak/akpivot)**enn*tk**2

      return 
      end
C
C
C
      subroutine SetPk(kid,aa)
      include 'cosmo.inc'
      include 'power.inc'
      include 'config.inc'
C
      common /tfdata1/ ndata, akdata(NMDATA), alkmin, alkmax, alkstp
      common /tfdata2/ tkdata(NMDATA)
      common /tfdata3/ ntime, aatime(NMTIME),
     .     txall(NMDATA,NMTIME), tball(NMDATA,NMTIME)
C
      if(kid.ge.1 .and. kid.le.kindIdMax) then
         kindId = kid
      else
         write(0,*) 'ERROR: incorrect type for a field: ', kid
         stop
      endif
C
      il = 0
      do i=1,ntime
         if(aatime(i) .lt. aa) il = il + 1
      enddo
      iu = il + 1

      if(il .lt. 1) il = 1
      if(iu .gt. ntime) iu = ntime

      if(iu .gt. il) then
         wu = (aa-aatime(il))/(aatime(iu)-aatime(il))
         wd = (aatime(iu)-aa)/(aatime(iu)-aatime(il))
      else
         wd = 1.0
         wu = 0.0
      endif

      if(kindId .eq. 1) then
         fx = OmX/OmM
         fb = OmB/OmM
C$OMP PARALLEL DO DEFAULT(NONE),
C$OMP+PRIVATE(j),
C$OMP+SHARED(ndata,tkdata,txall,tball,wu,wd,iu,il,fx,fb)
         do j=1,ndata
            tkdata(j) = 
     .           wu*log(fx*exp(txall(j,iu))+fb*exp(tball(j,iu))) +
     .           wd*log(fx*exp(txall(j,il))+fb*exp(tball(j,il)))
         enddo
      else if(kindId .eq. 2) then
C$OMP PARALLEL DO DEFAULT(NONE),
C$OMP+PRIVATE(j),
C$OMP+SHARED(ndata,tkdata,txall,wu,wd,iu,il)
         do j=1,ndata
            tkdata(j) = wu*txall(j,iu) + wd*txall(j,il)
         enddo
      else if(kindId .eq. 3) then
C$OMP PARALLEL DO DEFAULT(NONE),
C$OMP+PRIVATE(j),
C$OMP+SHARED(ndata,tkdata,tball,wu,wd,iu,il)
         do j=1,ndata
            tkdata(j) = wu*tball(j,iu) + wd*tball(j,il)
         enddo
      else
         write(0,*) 'ERROR: Bug in SetPk'
      endif
      
      Ampl = Ampl0
      aaLast = aa

      return
      end
C
C
C
      subroutine InitPower
      include 'cosmo.inc'
      include 'power.inc'
      include 'config.inc'
C
      common /tfdata1/ ndata, akdata(NMDATA), alkmin, alkmax, alkstp
      common /tfdata3/ ntime, aatime(NMTIME),
     .     txall(NMDATA,NMTIME), tball(NMDATA,NMTIME)
C
      iu = 0
      len = index(TFname,' ') - 1
      do i=1,len
         if(TFname(i:i) .eq. '.') iu = i
      enddo
      iu = iu + 1

      if(TFname(iu:len) .eq. 'lic') then
         call LoadLINGERFile
      else if(TFname(iu:len) .eq. 'psd') then
         call LoadSLHPSDFile
      else
         write(0,*) 'ERROR: Unknown TF file extension: ', TFname(iu:len)
         write(0,*) 'Valid extensions: lic psd'
         stop
      endif
C
C  Verify integrity
C
      alkmin = akdata(1)
      alkmax = akdata(ndata)
      alkstp = (alkmax-alkmin)/real(ndata-1)
      devmax = 0.0
      do l=1,ndata
         alkc = alkmin + alkstp*real(l-1)
         devmax = max(devmax,abs(alkc-akdata(l)))
      enddo
      if(devmax .gt. 1.0e-3) then
         write(6,*) 'Nonuniform TF file: ',devmax
         stop
      endif
C
C  Normalize
C
      do j=1,ntime
         fx0 = txall(1,j)
         fb0 = tball(1,j)
C$OMP PARALLEL DO DEFAULT(NONE),
C$OMP+PRIVATE(l),
C$OMP+SHARED(j,ndata,txall,tball,fx0,fb0)
         do l=1,ndata
            txall(l,j) = txall(l,j) - fx0
            tball(l,j) = tball(l,j) - fb0
         enddo
      enddo
C
      Ampl0 = 2*3.1415927**2*2.21e-9*(0.4*3000**2/OmM)**2*akpivot

      call SetPk(1,1.0)

      s8 = sigmaR(8.0)*Dplus(1.0)

      if(info .ge. 0) then
         write(6,*) 'Normalization is ', sigma8/s8, ' off WMAP5'
      endif

      if(sigma8 .gt. 0.0) then
         Ampl0 = Ampl0*(sigma8/s8)**2
      endif
      Ampl = 0.0

      return
      end
C
C
C
      subroutine LoadSLHPSDFile
      include 'cosmo.inc'
      include 'power.inc'
      include 'config.inc'
C
      common /tfdata1/ ndata, akdata(NMDATA), alkmin, alkmax, alkstp
      common /tfdata3/ ntime, aatime(NMTIME),
     .     txall(NMDATA,NMTIME), tball(NMDATA,NMTIME)
C
      ntime = 1
      aatime(1) = 1.0

      ib = index(TFname,' ') - 1
      open(unit=11, file=TFname(1:ib), status='old')

      ndata = 0
 101  read(11,*,end=102) alrc, txdc, tbdc
      ndata = ndata + 1
      akdata(ndata) = log(2.0*3.1415927) - log(10.0)*alrc
      txall(ndata,1) = log(txdc)
      tball(ndata,1) = log(tbdc)
      goto 101
 102  continue
      close(11)

      return
      end
C
C
C
      subroutine LoadLINGERFile
      include 'cosmo.inc'
      include 'power.inc'
      include 'config.inc'
C
      character*255 str
C
      common /tfdata1/ ndata, akdata(NMDATA), alkmin, alkmax, alkstp
      common /tfdata3/ ntime, aatime(NMTIME),
     .     txall(NMDATA,NMTIME), tball(NMDATA,NMTIME)
C
      ib = index(TFname,' ') - 1
      open(unit=11, file=TFname(1:ib), status='old')
      write(6,*) 'Reading TF file...'

      read(11,*) wB, wX, wL, wN
      call CheckParameter(wB,OmB,'OmegaB')
      call CheckParameter(wX,OmX,'OmegaX')
      call CheckParameter(wL,OmL,'OmegaL')
      call CheckParameter(wN,OmN,'OmegaN')

      read(11,*) hIn
      hIn = hIn*0.01
      call CheckParameter(hIn,h100,'h100')

      id = 0
      ntime = 0
      ndata = 0

 101  read(11,*,end=102) i, ak, aa, aux4, aux5, aux6, tx, tb
      tx = tx/ak**2
      tb = tb/ak**2

      if(i .eq. id) then
         ntime = ntime + 1
      else
         ntime = 1
      endif

      if(ntime .eq. 1) then
         id = i
         ndata = ndata + 1
         akdata(ndata) = log(ak/h100)
      endif
      if(ndata .eq. 1) then
         aatime(ntime) = aa
      else
         write(str,'(A1,I3)') 'a', ndata
         call CheckParameter(aa,aatime(ntime),str)
      endif
      
      txall(ndata,ntime) = log(tx)
      tball(ndata,ntime) = log(tb)

      goto 101

 102  continue

      close(11)

      return
      end
C
C
C
      subroutine CheckParameter(vIn,vMust,name)
      character*(*) name

      if(abs(vIn-vMust) .gt. 1.0e-6) then
         ib = index(name,' ')
         if(ib .eq. 0) ib = len(name)
         write(0,*) 'ERROR: invalid ', name(1:ib), '= ', vIn,
     .        '; must be ', vMust
         close(11)
         stop
      endif

      return
      end
C
C
C
      function SigmaR(r)
      dimension par(1)
      real funSR
      external funSR

      par(1) = r
      akmax = 0.0
      call IntegPar(funSR,1,0.0,akmax,3.1415927/r,1.0e-3,s,par)

      SigmaR = sqrt(0.5*s/3.1415927**2)

      return
      end
C
C
C
      function funSR(ak,par)
      dimension par(*)

      x = ak*par(1)
      if(x .lt. 1.0e-3) then
         w = 1.0 - x**2/10
      else
         w = 3.0*(sin(x)-x*cos(x))/x**3
      endif

      funSR = ak**2*Pk(ak)*w**2

      return
      end
C
C
C
      function SigmaL(r)
      dimension par(1)
      real funSL
      external funSL

      par(1) = r
      akmax = 0.0
      call IntegPar(funSL,1,0.0,akmax,3.1415927/r,1.0e-3,s,par)

      SigmaL = sqrt(0.5*s/3.1415927**2)

      return
      end
C
C
C
      function funSL(ak,par)
      dimension par(*)

      x = ak*par(1)
      w2 = exp(-(0.3*x)**2)  !! This is a fit to the exact Wa(kL) calculaiton

      funSL = ak**2*Pk(ak)*w2

      return
      end
C
C
C
      function xiInt(r)
      dimension par(1)
      real funXI
      external funXI

      par(1) = r
      akmax = 0.0
      call IntegPar(funXI,1,0.0,akmax,3.1415927/r,1.0e-3,s,par)
      
      xiInt = s/(2*3.1415927**2)

      return
      end
C
C
C
      function funXI(ak,par)
      dimension par(*)

      y = ak*par(1)
      if(y .lt. 1.0e-3) then
         w = 1.0 - y**2/6
      else
         w = sin(y)/y
      endif

      funXI = ak**2*Pk(ak)*w

      return
      end
C
C
C
      subroutine SetXiFit(rmin,rmax)
      include 'config.inc'
      parameter(NXIMAX = 999)
      parameter(NXIDEX = 20) !! 20 points per e-folding
      common /xidata1/ nxi, alrmin, alrmax, alrstp, xidata(NXIMAX)

      alrmin = log(1.0e-10+rmin)
      alrmax = log(1.0e-10+rmax)
      alrstp = 1.0/NXIDEX

      nxi = 1 + int((alrmax-alrmin)/alrstp)
      if(nxi .gt. NXIMAX) then
         write(0,*) 'ERROR: SetXiFit::NXIMAX is too small ',
     .        NXIMAX, nxi
         stop
      endif
      
      alrmax = alrmin + alrstp*(nxi-1)

      if(info .gt. 0) then
         write(6,*) '... preparing xi(r) table'
      endif

      do i=1,nxi
         r = exp(alrmin+alrstp*(i-1))
         xidata(i) = xiInt(r)*r
      enddo

      return
      end
C
C
C
      function xiFit(r)
      parameter(NXIMAX = 999)
      common /xidata1/ nxi, alrmin, alrmax, alrstp, xidata(NXIMAX)
C
      alr = log(r+1.0e-10)
      if(alr.lt.alrmin .or. alr.gt.alrmax) then
         xiFit = xiInt(r)
      else
         q = (alr-alrmin)/alrstp
         lpd = 1 + int(q)
         lpu = lpd + 1
         if(lpu .gt. nxi) then
            lpu = nxi
            lpd = nxi - 1
         endif
         wu = q - (lpd-1)
         wd = 1.0 - wu
         xiFit = (wu*xidata(lpu)+wd*xidata(lpd))/r
      endif

      return
      end

      subroutine ioOpenFile(id,name,mode)
      character*(*) name
      logical mode

      if(mode) then
         open(unit=id, file=name, form='unformatted')
      else
         open(unit=id, file=name, form='unformatted', status='old')
      endif

      return
      end
C
C
C
      subroutine ioCloseFile(id)

      close(id)

      return
      end
C
C
C
      subroutine ioWriteManifest(id,jobname,iver)
      character*256 jobname

      write(id) jobname, iver

      return
      end
C
C
C
      subroutine ioReadManifest(id,jobname,iver)
      character*256 jobname

      read(id) jobname, iver

      return
      end
C
C
C
      subroutine ioWriteRealRecord(id,n,buffer)
      dimension buffer(*)

      write(id) (buffer(i),i=1,n)

      return
      end
C
C
C
      subroutine ioReadRealRecord(id,n,buffer)
      dimension buffer(*)

      read(id) (buffer(i),i=1,n)

      return
      end
C
C
C
      subroutine ioWriteIntgRecord(id,n,ibuffer)
      dimension ibuffer(*)

      write(id) (ibuffer(i),i=1,n)

      return
      end
C
C
C
      subroutine ioReadIntgRecord(id,n,ibuffer)
      dimension ibuffer(*)

      read(id) (ibuffer(i),i=1,n)

      return
      end
C
C
C
      subroutine ioWriteFileHeader(id,OmB,OmX,OmL,OmN,h100,enn,sigma8,
     .     akpivot,aBegin,cell,delDC,rmsDC,nx,ny,nz,is,is2,nrec,np,
     .     idx,iflag)
      integer*8 np
      dimension extra(100)
      data extra/100*0.0/

      write(id) OmB, OmX, OmL, OmN, h100, enn, sigma8, akpivot, 
     .     aBegin, cell, delDC, rmsDC, nx, ny, nz, is, is2, nrec, np, 
     .     idx, iflag, extra

      return
      end
C
C
C
      subroutine ioReadFileHeader(id,OmB,OmX,OmL,OmN,h100,enn,sigma8,
     .     akpivot,aBegin,cell,delDC,rmsDC,nx,ny,nz,is,is2,nrec,np,
     .     idx,iflag)
      integer*8 np
      dimension extra(100)

      read(id) OmB, OmX, OmL, OmN, h100, enn, sigma8, akpivot, 
     .     aBegin, cell, delDC, rmsDC, nx, ny, nz, is, is2, nrec, np,
     .     idx, iflag, extra

      return
      end
C
C
C
      subroutine ioWriteLevelHeader(id,level,maxLevel,np,wp,ind)
      integer*8 np

      write(id) level, maxLevel, np, wp, ind

      return
      end
C
C
C
      subroutine ioReadLevelHeader(id,level,maxLevel,np,wp,ind)
      integer*8 np

      read(id) level, maxLevel, np, wp, ind

      return
      end

      subroutine fftInit(NX,NY,NZ)
      include 'fft.inc'

      call nfft3is(NX,NY,NZ,ws,NW)

      return
      end
C
C
C
      subroutine fftX2K(NX,NY,NZ,NDIM1,NDIM2,d)
      include 'fft.inc'
      dimension d(NDIM1,NDIM2,*)

      call nfft3sc(NX,NY,NZ,d,NDIM1,NDIM2,ws,NW)

      return
      end
C
C
C
      subroutine fftK2X(NX,NY,NZ,NDIM1,NDIM2,d)
      include 'fft.inc'
      dimension d(NDIM1,NDIM2,*)

      call nfft3cs(NX,NY,NZ,d,NDIM1,NDIM2,ws,NW)

      return
      end

      subroutine SetAmplitudes(lSave,NLOW1,NLOW2,dAmp)
      include 'dims.inc'
      include 'config.inc'
      dimension dAmp(NLOW1,NLOW2,*)
      logical lSave
C
C  Set amplitudes in real space
C
      if(info .ge. 0) then
         write(6,*) 'Computing amplitudes...'
      endif

      idum = iseed
      do kc=1,NLOWZ
         do jc=1,NLOWY
            do ic=1,NLOWX
               dAmp(ic,jc,kc) = gasdev(idum)
            enddo
         enddo
      enddo
C
C  Set the DC mode
C
      call SetPk(1,1.0)
      if(usePGrid) call ComputePGrid
      call SetDCMode(NLOW1,NLOW2,dAmp)
C
C  Saving the original amplitudes, if needed
C
      if(lSave) then
         label = 'lr'
         call WriteAmplitudes('amp',NLOWX,NLOWY,NLOWZ,NLOW1,NLOW2,dAmp)
      endif

      return
      end
C
C
C
      subroutine CreateLowResIC(lConfig,NLOW1,NLOW2,dAmp,dLow,
     .     dSave,file,dirty)
      include 'dims.inc'
      include 'config.inc'
C
C  Configuration parameters:
C  1.  Save POS file
C  2.  Save VEL file
C  3.  Save DEN file
C
      logical lConfig(3)
      dimension dAmp(NLOW1,NLOW2,*)
      dimension dLow(NLOW1,NLOW2,*)
      dimension dSave(NLOW1,NLOW2,*)
      character*(*) file
      logical dirty
C
      integer*8 nPart
      dimension ijk(3)
      character*256 FileName
      external FileName
C
      if(aBegin .lt. 0.0) then
         write(0,*) 'ERROR: starting time has not been computed.'
         stop
      endif
C
C  Initialize
C
      dirty = .true.
      if(usePGrid) call ComputePGrid
      call GetUnitFactors(dFac,xFac,vFac,wFac)

      label = 'lr'

      nPart = NLOWX*NLOWY
      nPart = nPart*NLOWZ
C
C  Set amplitudes in real space
C
      if(info .ge. 0) then
         write(6,*) 'Computing low-res realization...'
      endif

      do kc=1,NLOWZ
         do jc=1,NLOWY
            do ic=1,NLOWX
               dLow(ic,jc,kc) = dAmp(ic,jc,kc)
            enddo
         enddo
      enddo
C
C  Compute the Fourier transform of the density field
C
      call InitFFT(NLOWX,NLOWY,NLOWZ)
      call fftX2K(NLOWX,NLOWY,NLOWZ,NLOW1,NLOW2,dLow)

C$OMP PARALLEL DO DEFAULT(NONE),
C$OMP+PRIVATE(kc,kkc,jc,jjc,iic,icr,ici,dk),
C$OMP+SHARED(akmin,dLow)
      do kc=1,NLOWZ
         kkc = kc - 1
         if(kkc .ge. NLOWZ/2) kkc = kkc - NLOWZ
         do jc=1,NLOWY
            jjc = jc - 1
            if(jjc .ge. NLOWY/2) jjc = jjc - NLOWY
            do iic=0,NLOWX/2

               icr = 2*iic + 1
               ici = 2*iic + 2

               dk = AmpK(iic,jjc,kkc)

               dLow(icr,jc,kc) = dk*dLow(icr,jc,kc)
               dLow(ici,jc,kc) = dk*dLow(ici,jc,kc)

            enddo
         enddo
      enddo
      dirty = .false.

      call SaveAmplitudes(NLOWX,NLOWY,NLOWZ,NLOW1,NLOW2,dLow,
     .     dSave,file,dirty)
C
C  Compute power spectrum for checking
C
      if(info .ge. 0) then
         write(6,*) 'Computing actual power spectrum...'
         call ComputePS(NLOWX,NLOWY,NLOWZ,NLOW1,NLOW2,dLow,'lps')
      endif

      if(info .gt. 1) then
         call ComputeSigma(R8,NLOWX,NLOWY,NLOWZ,NLOW1,NLOW2,dLow,s,
     .        dSave,file,dirty)
         write(6,*) 'Measured sigma8: ', s*Dplus(1.0)
         call CheckStartEpoch(0,NLOWX,NLOWY,NLOWZ,NLOW1,NLOW2,dLow,abeg,
     .        dSave,file,dirty)
      endif
C
C  Create POS/VEL files if needed
C
      if(lConfig(1) .or. lConfig(2)) then

         if(info .ge. 0) then
            write(6,*) 'Generating displacements...'
         endif

         if(lConfig(1)) then
            call OpenOutputFile(21,'pos',IOWRITE,NLOWX,NLOWY,NLOWZ,
     .           nPart,0)
         endif
         if(lConfig(2)) then
            call OpenOutputFile(22,'vel',IOWRITE,NLOWX,NLOWY,NLOWZ,
     .           nPart,0)
         endif

         rmsDisp = 0.0
C
C  Loop over 3 spatial directions
C
         do iDir=1,3

            call ComputeDisplacement(iDir,NLOWX,NLOWY,NLOWZ,
     .           NLOW1,NLOW2,dLow,
     .           dSave,file,dirty)

            if(info .gt. 0) then
               call ComputeStat(NLOWX,NLOWY,NLOWZ,NLOW1,NLOW2,dLow,
     .              ss1,ss2)
               rmsDisp = rmsDisp + ss2**2
            endif
C
C  Save velocities     
C
            if(lConfig(2)) then
               call WriteArray(22,NLOWX,NLOWY,NLOWZ,NLOW1,NLOW2,
     .              dLow,vFac)
            endif

            if(info .gt. 0) then
               write(6,*) '... pec velocity (avg,rms): ', vFac*ss1, 
     .              vFac*ss2
            endif
C
C  Save positions     
C
            if(info .gt. 0) then
               write(6,*) '... displacement (avg,rms): ', xFac*ss1,
     .              xFac*ss2
            endif

            if(lConfig(1)) then

C$OMP PARALLEL DO DEFAULT(NONE),
C$OMP+PRIVATE(kc,jc,ic,ijk),
C$OMP+SHARED(dLow,cell,iDir,xFac)
               do kc=1,NLOWZ
                  ijk(3) = kc
                  do jc=1,NLOWY
                     ijk(2) = jc
                     do ic=1,NLOWX
                        ijk(1) = ic
                        dLow(ic,jc,kc) = cell*(ijk(iDir)-0.5) + 
     .                       xFac*dLow(ic,jc,kc)
                     enddo
                  enddo
               enddo
               
               call WriteArray(21,NLOWX,NLOWY,NLOWZ,NLOW1,NLOW2,
     .              dLow,1.0)

            endif

         enddo

         if(info .gt. 0) then
            rmsDisp = sqrt(rmsDisp)
            write(6,*) 'RMS displacement per cell: ', rmsDisp*xFac/cell
         endif

         if(lConfig(1)) then
            call ioCloseFile(21)
         endif
         if(lConfig(2)) then
            call ioCloseFile(22)
         endif

      endif
C
C  Create DEN file if needed
C
      if(lConfig(3)) then

         if(info .ge. 0) then
            write(6,*) 'Generating overdensity...'
         endif

         call OpenOutputFile(23,'den',IOWRITE,NLOWX,NLOWY,NLOWZ,nPart,0)

         call ComputeOverdensity(NLOWX,NLOWY,NLOWZ,NLOW1,NLOW2,dLow,
     .        dSave,file,dirty)
      
         call WriteArray(23,NLOWX,NLOWY,NLOWZ,NLOW1,NLOW2,dLow,dFac)
         
         if(info .gt. 0) then
            call ComputeStat(NLOWX,NLOWY,NLOWZ,NLOW1,NLOW2,dLow,
     .           ss1,ss2)
            write(6,*) '... overdensity (avg,rms): ', dFac*ss1, dFac*ss2
         endif

         call ioCloseFile(23)

      endif

      return
      end

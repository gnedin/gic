      subroutine HighResDriver(arg,lConfig,NLOW1,NLOW2,dLow,mask,levOff,
     .     NHIGH1,NHIGH2,dHigh,
     .     dSave,file,dirty)
      include 'dims.inc'
      include 'config.inc'
      character*256 arg(*)
      dimension dLow(NLOW1,NLOW2,*)
      dimension mask(NLOW1,NLOW2,*), ish(3)
      dimension dHigh(NHIGH1,NHIGH2,*)
      dimension dSave(NHIGH1,NHIGH2,*)
      character*(*) file
      logical dirty, lConfig(*)
C
      call SetPk(1,1.0)
      if(lConfig(5)) then
         call LoadLowResMask(NLOW1,NLOW2,mask,ish,levOff)
      else
         do i=1,3
            ish(i) = 0
         enddo
      endif
      call LoadLowResData(NLOW1,NLOW2,dLow,ish)
      call ComputeStartEpoch(LREF)

      do i=1,3
         if(arg(i) .eq. 'set') then
            call SetPk(i,aBegin)
            call CreateHighResIC(lConfig,NLOW1,NLOW2,dLow,mask,ish,
     .           NHIGH1,NHIGH2,dHigh,
     .           dSave,'tmp',dirty)
         endif
      enddo

      return
      end
C
C
C
      subroutine LoadLowResMask(NLOW1,NLOW2,mask,ish,levOff)
      include 'dims.inc'
      include 'config.inc'
      dimension mask(NLOW1,NLOW2,*), ish(3)
C
C  Special case - if levOff is negative, make a uniform mask to -levOff
C
      if(levOff .lt. 0) then

         do k=1,NLOWZ
            do j=1,NLOWY
               do i=1,NLOWX
                  mask(i,j,k) = -levOff
               enddo
            enddo
         enddo

         do i=1,3
            ish(i) = 0
         enddo

      else

         isl = index(sublabel,' ') - 1
         if(isl .gt. 0) then
            label = 'lr'//sublabel(1:isl)
         else
            label = 'lr'
         endif

         kPrev = kindId
         kindId = 1
 
         call OpenFile(99,'rfm',IOREAD)
         call ReadMask(99,NLOWX,NLOWY,NLOWZ,NLOW1,NLOW2,mask,ish)
         call ioCloseFile(99)

         kindId = kPrev
C     
C  Clamp the mask
C     
         do k=1,NLOWZ
            do j=1,NLOWY
               do i=1,NLOWX
                  mask(i,j,k) = min(LREF,max(0,mask(i,j,k)-levOff))
               enddo
            enddo
         enddo

      endif
C
C  Clamp the max level
C
      LREF = 0
      do k=1,NLOWZ
         do j=1,NLOWY
            do i=1,NLOWX
               LREF = max(LREF,mask(i,j,k))
            enddo
         enddo
      enddo
      return
      end
C
C
C
      subroutine LoadLowResData(NLOW1,NLOW2,dLow,ish)
      include 'dims.inc'
      include 'config.inc'
      dimension dLow(NLOW1,NLOW2,*), ish(3)
C
C  Read low res data
C
      label = 'lr'
      kPrev = kindId
      kindId = 1

      call OpenFile(99,'amp',IOREAD)
      call ReadArrayShifted(99,NLOWX+2,NLOWY,NLOWZ,NLOW1,NLOW2,dLow,ish)
      call ioCloseFile(99)

      kindId = kPrev
C
C  Set the DC mode
C
      if(usePGrid) call ComputePGrid
      call SetDCMode(NLOW1,NLOW2,dLow)

      return
      end
C
C
C
      subroutine CreateHighResIC(lConfig,NLOW1,NLOW2,dLow,mask,ish,
     .     NHIGH1,NHIGH2,dHigh,
     .     dSave,file,dirty)
      include 'dims.inc'
      include 'config.inc'
C
C  Configuration parameters:
C  1.  Save POS file
C  2.  Save VEL file
C  3.  Save DEN file
C  4.  Create data on all levels
C  5.  Create multi-resolution data with the mask
C  6.  Save index file for multi-resolution data
C  7.  Downgrade mask by a numberSave index file for multi-resolution data
C
      logical lConfig(6)
      dimension dLow(NLOW1,NLOW2,*)
      dimension mask(NLOW1,NLOW2,*), ish(3)
      dimension dHigh(NHIGH1,NHIGH2,*)
      dimension dSave(NHIGH1,NHIGH2,*)
      character*(*) file
      logical dirty
C
      dimension amp(8)
C
      if(aBegin .lt. 0.0) then
         write(0,*) 'ERROR: starting time has not been computed.'
         stop
      endif
C
C  Initialize
C
      dirty = .true.
      if(usePGrid) call ComputePGrid
      call GetUnitFactors(dFac,xFac,vFac,wFac)
C
      NX = NLOWX
      NY = NLOWY
      NZ = NLOWZ
C
C  If all levels are requested, do unrefined level 0 first
C
      if(lConfig(4) .or. lConfig(5)) then
C
C  Set amplitudes (they will be destroyed inside MakeOneLevel)
C
C$OMP PARALLEL DO DEFAULT(NONE),
C$OMP+PRIVATE(kc,jc,ic),
C$OMP+SHARED(dHigh,dLow,NX,NY,NZ)
         do kc=1,NZ
            do jc=1,NY
               do ic=1,NX
                  dHigh(ic,jc,kc) = dLow(ic,jc,kc)
               enddo
            enddo
         enddo
         dirty = .false.

         call MakeOneLevel(0,lConfig,NX,NY,NZ,NLOW1,NLOW2,mask,ish,
     .        NHIGH1,NHIGH2,dHigh,dFac,xFac,vFac,wFac,
     .        dSave,file,dirty)

      endif
C
C  Start refinement
C
      fac1 = sqrt(0.125)
      NR = 2**LREF

C$OMP PARALLEL DO DEFAULT(NONE),
C$OMP+PRIVATE(kc,jc,ic),
C$OMP+SHARED(dHigh,dLow,NR)
      do kc=1,NLOWZ
         do jc=1,NLOWY
            do ic=1,NLOWX
               dHigh(ic,jc,kc) = dLow(ic,jc,kc)
            enddo
         enddo
      enddo
      dirty = .false.

      idum = iseed2

      do LEV=1,LREF

         if(info .gt. 0) then
            write(6,*) 'Refining level: ', LEV
         endif

         NR2 = NR/2

         if(info .gt. 99) then
            write(6,*) '... NR, NR2, NX: ', NR, NR2, NX
         endif

         if(dirty) then
            write(0,*) 'ERROR: Bug in CreateHighResIC: ',
     .           'dirty amplitudes.'
            stop
         endif
C
C  Serial loop b/c of gasdev call; fill the array from back to front,
C  to avoid overwriting LLF corner with the data.
C
         do kc=NZ,1,-1
            do jc=NY,1,-1
               do ic=NX,1,-1
C
C  Get constrained amplitudes
C
                  do l=1,8
                     amp(l) = gasdev(idum)
                  enddo

                  s = 0.0
                  do l=1,8
                     s = s + amp(l)
                  enddo

                  s = fac1*dHigh(ic,jc,kc) - 0.125*s

                  do l=1,8
                     amp(l) = amp(l) + s
                  enddo

                  dHigh(2*ic-1,2*jc-1,2*kc-1) = amp(1)
                  dHigh(2*ic-0,2*jc-1,2*kc-1) = amp(2)
                  dHigh(2*ic-1,2*jc-0,2*kc-1) = amp(3)
                  dHigh(2*ic-0,2*jc-0,2*kc-1) = amp(4)
                  dHigh(2*ic-1,2*jc-1,2*kc-0) = amp(5)
                  dHigh(2*ic-0,2*jc-1,2*kc-0) = amp(6)
                  dHigh(2*ic-1,2*jc-0,2*kc-0) = amp(7)
                  dHigh(2*ic-0,2*jc-0,2*kc-0) = amp(8)

               enddo
            enddo
         enddo
         dirty = .false.

         NR = NR2
         NX = 2*NX
         NY = 2*NY
         NZ = 2*NZ

         if(info .gt. 1) then
            call ComputeStat(NX,NY,NZ,NHIGH1,NHIGH2,dHigh,ss1,ss2)
            write(6,*) '... avg,rms amplitudes: ', ss1, ss2
         endif

         if((lConfig(4) .or. lConfig(5)) .and. LEV.lt.LREF) then
C
C  If we are doing intermediate levels, save current amplitudes in the
C  second half of the data array
C
C$OMP PARALLEL DO DEFAULT(NONE),
C$OMP+PRIVATE(kc,jc,ic),
C$OMP+SHARED(dHigh,NX,NY,NZ)
            do kc=1,NZ
               do jc=1,NY
                  do ic=1,NX
                     dHigh(ic,jc,NHIGHZ/2+kc) = dHigh(ic,jc,kc)
                  enddo
               enddo
            enddo

         endif

         if((lConfig(4) .or. lConfig(5)) .or. LEV.eq.LREF) then

            call MakeOneLevel(LEV,lConfig,NX,NY,NZ,NLOW1,NLOW2,mask,ish,
     .           NHIGH1,NHIGH2,dHigh,dFac,xFac,vFac,wFac,
     .           dSave,file,dirty)

         endif

         if((lConfig(4) .or. lConfig(5)) .and. LEV.lt.LREF) then
C
C  If we are doing intermediate levels, restore current amplitudes from the
C  second half of the data array
C
C$OMP PARALLEL DO DEFAULT(NONE),
C$OMP+PRIVATE(kc,jc,ic),
C$OMP+SHARED(dHigh,NX,NY,NZ)
            do kc=1,NZ
               do jc=1,NY
                  do ic=1,NX
                     dHigh(ic,jc,kc) = dHigh(ic,jc,NHIGHZ/2+kc)
                  enddo
               enddo
            enddo
            dirty = .false.

         endif

      enddo

      return
      end
C
C
C
      subroutine MakeOneLevel(LEV0,lConfig,NX,NY,NZ,NLOW1,NLOW2,mask,
     .     ish,NHIGH1,NHIGH2,dHigh,dFac,xFac,vFac,wFac0,
     .     dSave,file,dirty)
      include 'dims.inc'
      include 'power.inc'
      include 'config.inc'
C
      logical lConfig(6)
      dimension mask(NLOW1,NLOW2,*), ish(3)
      dimension dHigh(NHIGH1,NHIGH2,*)
      dimension dSave(NHIGH1,NHIGH2,*)
      character*(*) file
      logical dirty
C
      integer*8 nPart, nPartTot
      logical lOpenFiles, lCloseFiles
      dimension ijk(3), ibuffer(3*NRECORD)
      character*256 FileName
      external FileName
C
      LEV = LEV0  !! this is to placate ftncheck
      NREF = 2**LEV
      NREF3 = NREF**3

      wFac = wFac0/NREF3
C
C  Count the number of particles in this level and the total number.
C
      if(lConfig(5)) then
         nPart = 0
         nPartTot = 0
C$OMP PARALLEL DO DEFAULT(NONE),
C$OMP+PRIVATE(kc0,jc0,ic0),
C$OMP+SHARED(dLow,mask,lev,nref3),
C$OMP+REDUCTION(+:nPart,nPartTot)
         do kc0=1,NLOWZ
            do jc0=1,NLOWY
               do ic0=1,NLOWX
                  nPartTot = nPartTot + 8**mask(ic0,jc0,kc0)
                  if(mask(ic0,jc0,kc0) .eq. LEV) then
                     nPart = nPart + NREF3
                  endif
               enddo
            enddo
         enddo
      else
         nPart = NX*NY
         nPart = nPart*NZ
         nPartTot = nPart
      endif

      if(info .ge. 0) then
         write(6,*) 'Level: ', LEV, nPart, nPartTot
         write(6,*) 'Particle mass: ', wFac
      endif
C
C  Decide how we open/close files.
C
C  Open files in MR if: doing first level
C  Open files in HR if: always, this is called when needed only
C
      if(lConfig(5)) then
         lOpenFiles = (LEV .eq. 0)
      else
         lOpenFiles = .true.
      endif
C
C  Close files in MR if: doing last level
C  Close files in HR if: always, this is called when needed only
C
      if(lConfig(5)) then
         lCloseFiles = (LEV .eq. LREF)
      else
         lCloseFiles = .true.
      endif
C
      if(lOpenFiles) then

         cellsav = cell

         if(lConfig(5)) then
            isl = index(sublabel,' ') - 1
            if(isl .gt. 0) then
               label = 'mr'//CHAR(48+LREF)//sublabel(1:isl)
            else
               label = 'mr'//CHAR(48+LREF)
            endif
            ifl = LREF
         else
            label = 'hr'//CHAR(48+LEV)
            ifl = 0
C
C  This is so that ...hr... files look like lr files, with their 
C  own value of cell
C
            cell = cell/NREF
         endif

         if(lConfig(1)) then
            call OpenOutputFile(21,'pos',IOWRITE,NX,NY,NZ,nPartTot,ifl)
            if(lConfig(5)) then
               call WriteMask(21,NLOWX,NLOWY,NLOWZ,NLOW1,NLOW2,mask,ish)
            endif
         endif

         if(lConfig(2)) then
            call OpenOutputFile(22,'vel',IOWRITE,NX,NY,NZ,nPartTot,ifl)
            if(lConfig(5)) then
               call WriteMask(22,NLOWX,NLOWY,NLOWZ,NLOW1,NLOW2,mask,ish)
            endif
         endif

         if(lConfig(3)) then
            call OpenOutputFile(23,'den',IOWRITE,NX,NY,NZ,nPartTot,ifl)
            if(lConfig(5)) then
               call WriteMask(23,NLOWX,NLOWY,NLOWZ,NLOW1,NLOW2,mask,ish)
            endif
         endif

         cell = cellsav

      endif

      if(info.gt.3 .and. max(NX,NY,NZ).le.1024) then
         open(unit=41, file=FileName('a.bin'), form='unformatted')
         write(41) NX, NY, NZ
         write(41) (((dHigh(ic,jc,kc),ic=1,NX),jc=1,NY),kc=1,NZ)
         close(41)
      endif
C
C  Compute the Fourier transform of the density field
C
      call InitFFT(NX,NY,NZ)
      call fftX2K(NX,NY,NZ,NHIGH1,NHIGH2,dHigh)
C
C  Add small-scale power
C
      fac2 = 0.5**(1.5*LEV)
C$OMP PARALLEL DO DEFAULT(NONE),
C$OMP+PRIVATE(kc,jc,iic,kkc,jjc,icr,ici,dk),
C$OMP+SHARED(dHigh,NX,NY,NZ,fac2)
      do kc=1,NZ
         kkc = kc - 1
         if(kkc .ge. NZ/2) kkc = kkc - NZ
         do jc=1,NY
            jjc = jc - 1
            if(jjc .ge. NY/2) jjc = jjc - NY
            do iic=0,NX/2

               icr = 2*iic + 1
               ici = 2*iic + 2

               dk = fac2*AmpK(iic,jjc,kkc)

               dHigh(icr,jc,kc) = dk*dHigh(icr,jc,kc)
               dHigh(ici,jc,kc) = dk*dHigh(ici,jc,kc)

            enddo
         enddo
      enddo
      dirty = .false.

      call SaveAmplitudes(NX,NY,NZ,NHIGH1,NHIGH2,dHigh,
     .     dSave,file,dirty)
C
C  Compute power spectrum for checking
C
      if(info .ge. 0) then
         write(6,*) 'Computing actual power spectrum...'
         call ComputePS(NX,NY,NZ,NHIGH1,NHIGH2,dHigh,'lps')
      endif

      if(info .gt. 1) then
         call ComputeSigma(R8,NX,NY,NZ,NHIGH1,NHIGH2,dHigh,s,
     .        dSave,file,dirty)
         write(6,*) 'Measured sigma8: ', s*Dplus(1.0)
         call CheckStartEpoch(LEV,NX,NY,NZ,NHIGH1,NHIGH2,dHigh,abeg,
     .        dSave,file,dirty)
      endif
C
C  Save POS/VEL data if needed
C
      if(lConfig(1) .or. lConfig(2)) then

         if(info .ge. 0) then
            write(6,*) 'Generating displacements...'
         endif

         if(lConfig(5)) then
            if(lConfig(1)) then
               call ioWriteLevelHeader(21,LEV,LREF,nPart,wFac,0)
            endif
            if(lConfig(2)) then
               call ioWriteLevelHeader(22,LEV,LREF,nPart,wFac,0)
            endif
         endif

         rmsDisp = 0.0
C
C  Loop over 3 spatial directions
C
         do iDir=1,3

            call ComputeDisplacement(iDir,NX,NY,NZ,NHIGH1,NHIGH2,dHigh,
     .           dSave,file,dirty)

            if(info .gt. 0) then
               call ComputeStat(NX,NY,NZ,NHIGH1,NHIGH2,dHigh,ss1,ss2)
               rmsDisp = rmsDisp + ss2**2
            endif
C
C  Save velocities     
C
            if(lConfig(2)) then
               if(lConfig(5)) then
                  call WriteArrayLevel(22,NX,NY,NZ,NHIGH1,NHIGH2,
     .                 dHigh,vFac,LEV,NLOW1,NLOW2,mask,.false.)
               else
                  call WriteArray(22,NX,NY,NZ,NHIGH1,NHIGH2,
     .                 dHigh,vFac)
               endif
            endif

            if(info .gt. 0) then
               write(6,*) '... pec velocity (avg,rms): ', vFac*ss1, 
     .              vFac*ss2
            endif
C
C  Save positions     
C
            if(info .gt. 0) then
               write(6,*) '... displacement (avg,rms): ', xFac*ss1,
     .              xFac*ss2
            endif

            if(lConfig(1)) then

C$OMP PARALLEL DO DEFAULT(NONE),
C$OMP+PRIVATE(kc,jc,ic,ijk),
C$OMP+SHARED(NX,NY,NZ,dHigh,cell,NREF,iDir,xFac)
               do kc=1,NZ
                  ijk(3) = kc
                  do jc=1,NY
                     ijk(2) = jc
                     do ic=1,NX
                        ijk(1) = ic
                        dHigh(ic,jc,kc) = cell/NREF*(ijk(iDir)-0.5) + 
     .                       xFac*dHigh(ic,jc,kc)
                     enddo
                  enddo
               enddo

               if(lConfig(5)) then
                  call WriteArrayLevel(21,NX,NY,NZ,NHIGH1,NHIGH2,
     .                 dHigh,1.0,LEV,NLOW1,NLOW2,mask,.false.)
               else
                  call WriteArray(21,NX,NY,NZ,NHIGH1,NHIGH2,
     .                 dHigh,1.0)
               endif

            endif

         enddo

         if(info .gt. 0) then
            rmsDisp = sqrt(rmsDisp)
            write(6,*) 'RMS displacement per cell: ', rmsDisp*xFac/cell
         endif

      endif
C
C  Create DEN file if needed
C
      if(lConfig(3)) then

         if(info .ge. 0) then
            write(6,*) 'Generating overdensity...'
         endif

         call ComputeOverdensity(NX,NY,NZ,NHIGH1,NHIGH2,dHigh,
     .        dSave,file,dirty)
      
         if(lConfig(5)) then
            if(lConfig(6)) then
               call ioWriteLevelHeader(23,LEV,LREF,nPart,wFac,1)
            else
               call ioWriteLevelHeader(23,LEV,LREF,nPart,wFac,1)
            endif
            call WriteArrayLevel(23,NX,NY,NZ,NHIGH1,NHIGH2,dHigh,
     .           dFac,LEV,NLOW1,NLOW2,mask,lConfig(6))
         else
            call WriteArray(23,NX,NY,NZ,NHIGH1,NHIGH2,dHigh,
     .           dFac)
         endif

         if(info .gt. 0) then
            call ComputeStat(NX,NY,NZ,NHIGH1,NHIGH2,dHigh,ss1,ss2)
            write(6,*) '... overdensity (avg,rms): ', dFac*ss1, dFac*ss2
         endif

      endif

      if(lCloseFiles) then
         if(lConfig(1)) then
            call ioCloseFile(21)
         endif
         if(lConfig(2)) then
            call ioCloseFile(22)
         endif
         if(lConfig(3)) then
            call ioCloseFile(23)
         endif
      endif

      return
      end

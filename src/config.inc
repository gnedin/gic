	character*256 dir, jobname, label, sublabel, TFname
	common /config1/ dir, jobname, label, sublabel, TFname
	common /config2/ info, iseed, iseed2
	logical useDisk, usePGrid
	common /config3/ useDisk, usePGrid
	parameter(kindIdMax = 3)
        character*1 kindNames(kindIdMax)
        common /config4/ kindId, kindNames
C
	common /config5/ aBegin, epsCell, delDC, rmsDC
C
C  Units
C
	common /config6/ cell, cell3, box(3), akmin(3), akNyq, R8
C
C  File modes
C
	logical IOREAD, IOWRITE
	parameter(IOREAD = .false.)
	parameter(IOWRITE = .true.)
	common /config7/ NRECORD

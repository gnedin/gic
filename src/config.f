      subroutine Init
      include 'dims.inc'
      include 'cosmo.inc'
      include 'power.inc'
      include 'config.inc'
C
      parameter (twopi = 2*3.1415927)
C
      kindNames(1) = 'M'
      kindNames(2) = 'D'
      kindNames(3) = 'B'

      NRECORD = 1024

      LREF = NumRef

      usePGrid = .true.
      useDisk = .false.
      aBegin = -1.0
      epsCell = 0.1
      iseed2 = 0

      call InitParameters

      delDC = 0.0
      rmsDC = 0.0
      akpivot = 0.02/h100
      cell3 = cell**3
      R8 = 8.0/cell

      if(iseed2 .eq. 0) iseed2 = iseed

      call InitCosmo
C
C  Scales and other units
C
      box(1) = NLOWX
      box(2) = NLOWY
      box(3) = NLOWZ

      do j=1,3
         akmin(j) = twopi/box(j)
      enddo

      akNyq = 0.5*twopi

      call InitPower  
C
C  Test gaussrand (had issues before)
C
      idum = iseed
      ga = 0.0
      gd = 0.0
      ng = 1000
      do i=1,ng
         g = gasdev(idum)
         ga = ga + g
         gd = gd + g**2
      enddo
      ga = ga/ng
      gd = sqrt(gd/ng-ga**2)
      fa = 0.5*sqrt(real(ng))
      if(fa*abs(ga).gt.1.0 .or. fa*abs(gd-1.0).gt.1.0) then
         write(0,*) 'ERROR: broken random number generator: ',
     .        ga, gd, ng
         stop
      endif

      return
      end
C
C
C
      subroutine ReadCommandLine(ioff,n,nreq,arg,opt,help,cmdhelp)
      dimension ind(n)
      character*(*) opt(n), arg(n), help(n), cmdhelp
C
      character*255 str
C
      do i=1,n
         arg(i) = ' '
      enddo

      if(iargc() .le. ioff) then
         iok = 0
      else
         iok = 1

         ireq = 0
         do j=ioff+1,iargc()
            call getarg(j,str)
            ib = index(str,' ') - 1
            ie = index(str,'=')
            io = ib
            if(ie .gt. 2) io = ie - 1
            ifound = 0
            do i=1,n
               i1 = index(opt(i),'$') - 1
               if(i1 .le. 0) then
                  write(0,*) 'ERROR: incorrectly set option ', opt(i)
                  stop
               endif
               if(str(1:io) .eq. '-'//opt(i)(1:i1)) then
                  ifound = i
                  if(ie.le.2 .or. ie.eq.ib) then
                     arg(i) = 'set'
                  else
                     arg(i) = str((ie+1):ib)
                     if(opt(i)((2+i1):(2+i1)) .ne. '=') iok = 0
                  endif
               endif
            enddo
            if(ifound .eq. 0) then
               iok = 0
            else if(ifound .le. nreq) then
               ireq = 1
            endif
         enddo

         if(nreq.gt.0 .and. ireq.eq.0) iok = 0

      endif
C
      if(iok .eq. 0) then
         call getarg(0,str)
         ib = index(str,' ') - 1

         if(nreq .gt. 0) then
            if(ioff .eq. 0) then
               write(6,*) 'Usage: ', str(1:ib), 
     .              ' <option> [ option ... ]'
            else
               write(6,*) 'Usage: ', str(1:ib), ' ', cmdhelp, 
     .              ' <option> [ option ... ]'
            endif
         else
            if(ioff .eq. 0) then
               write(6,*) 'Usage: ', str(1:ib), 
     .              ' [ option ... ]'
            else
               write(6,*) 'Usage: ', str(1:ib), ' ', cmdhelp, 
     .              ' [ option ... ]'
            endif
         endif
         write(6,*) 'where option is one of the following:'
         do i=1,n
            i1 = index(opt(i),'$') - 1
            i2 = index(help(i),'$') - 1
            if(opt(i)((2+i1):(2+i1)) .ne. '=') then
               write(6,*) '  -', opt(i)(1:i1), ' : ', help(i)(1:i2)
            else
               write(6,*) '  -', opt(i)(1:i1), '[=<arg>] : ', 
     .              help(i)(1:i2)
            endif
            if(i .eq. nreq) then
               write(6,*) '** One of the above options is required. **'
            endif
         enddo
         stop
      endif

      if(.false.) then
         do i=1,n
            i1 = index(opt(i),'$') - 1
            i2 = index(arg(i),' ') - 1
            write(6,*) opt(i)(1:i1), ' : ', arg(i)(1:i2)
         enddo
      endif

      return
      end
C
C
C
      subroutine InterpretRootString(str)
      include 'config.inc'
      character*(*) str

      len = index(str,' ') - 1

      i1 = 0
      i2 = 0
      do i=1,len
         if(str(i:i) .eq. '_') then
            i2 = i1
            i1 = i
         endif
      enddo

      if(i1.eq.0 .or. i2.eq.0) then
         write(0,*) 'Argument ', str(1:len), ' is not a valid file ',
     .        'name root.'
         stop
      endif

      if(mod(index('lrhrmr',str((i2+1):(i2+2))),2) .ne. 1) i1 = 0
      if(index('_0123456789',str((i2+3):(i2+3))) .eq. 0) i1 = 0

      kindId = index('MDB',str((i1+1):(i1+1)))
      if(kindId .eq. 0) i1 = 0

      if(i1 .eq. 0) then
         write(0,*) 'Argument ', str(1:len), ' is not a valid file ',
     .        'name root.'
         stop
      endif

      jobname = str(1:(i2-1))
      label = str((i2+1):(i1-1))

      il = index(label,' ') - 1
      ib = index(jobname,' ') - 1
      if(info .gt. 0) then
         write(6,*) 'Loading job/label: ', jobname(1:ib), '/', 
     .        label(1:il)
      endif

      return
      end

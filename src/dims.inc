	include '../setup.inc'
C
	parameter(NLOWX = NumLowX)
	parameter(NLOWY = NumLowY)
	parameter(NLOWZ = NumLowZ)
C
	parameter(NHIGHX = NLOWX*2**NumRef)
	parameter(NHIGHY = NLOWY*2**NumRef)
	parameter(NHIGHZ = NLOWZ*2**NumRef)
C
	parameter(NGRID0 = 16)
	parameter(NGRIDX = min(NGRID0,NLOWX))
	parameter(NGRIDY = min(NGRID0,NLOWY))
	parameter(NGRIDZ = min(NGRID0,NLOWZ))
C
	parameter(wLow = 1.0/real(NLOWX)/real(NLOWY)/real(NLOWZ))
	parameter(wHigh = 1.0/real(NHIGHX)/real(NHIGHY)/real(NHIGHZ))
	parameter(wGrid = 1.0/real(NGRIDX)/real(NGRIDY)/real(NGRIDZ))
C
	common /dims1/ LREF

 

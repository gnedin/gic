      function AmpK(iic,jjc,kkc)
      include 'dims.inc'
      include 'config.inc'
C
      common /pgrid1/ pGrid(NGRIDX+2,NGRIDY,NGRIDZ)
C
      if(usePGrid .and. iic.le.NGRIDX/2 .and. 
     .     jjc.ge.-NGRIDY/2 .and. jjc.lt.NGRIDY/2 .and.
     .     kkc.ge.-NGRIDZ/2 .and. kkc.lt.NGRIDZ/2 ) then
         ig = iic + 1
         jg = jjc + 1
         if(jg .lt. 1) jg = jg + NGRIDY
         kg = kkc + 1
         if(kg .lt. 1) kg = kg + NGRIDZ
         AmpK = wLow*(pGrid(2*ig-1,jg,kg)**2+pGrid(2*ig,jg,kg)**2)**0.25
      else
         akz = akmin(3)*real(kkc)
         aky = akmin(2)*real(jjc)
         akx = akmin(1)*real(iic)
         ak1 = sqrt(akx**2+aky**2+akz**2)
         AmpK = wLow*sqrt(Pk(ak1/cell)/cell3)
      endif

      return 
      end
C
C
C
      subroutine ComputePGrid
      include 'dims.inc'
      include 'config.inc'
C
      parameter(NGRID1 = NGRIDX + 2)
      parameter(NGRID2 = NGRIDY)
C
      common /pgrid1/ pGrid(NGRID1,NGRID2,NGRIDZ)
C
      if(info .ge. 0) then
         write(6,*) 'Computing PGrid...'
      endif
C
C  Compute xi0 first, per Ue-Li instructions
C
C$OMP PARALLEL DO DEFAULT(NONE),
C$OMP+PRIVATE(kc,kkc,jc,jjc,ic,akz,aky,akx,icr,ici,ak1),
C$OMP+SHARED(akmin,pGrid,cell,cell3)
      do kc=1,NGRIDZ
         kkc = kc - 1
         if(kkc .ge. NGRIDZ/2) kkc = kkc - NGRIDZ
         akz = akmin(3)*real(kkc)
         do jc=1,NGRIDY
            jjc = jc - 1
            if(jjc .ge. NGRIDY/2) jjc = jjc - NGRIDY
            aky = akmin(2)*real(jjc)
            do ic=1,NGRIDX/2+1
               akx = akmin(1)*real(ic-1)

               icr = 2*ic-1
               ici = 2*ic

               ak1 = sqrt(akx**2+aky**2+akz**2)

               pGrid(icr,jc,kc) = sqrt(Pk(ak1/cell)/cell3)
               pGrid(ici,jc,kc) = 0.0

            enddo
         enddo
      enddo

      call InitFFT(NGRIDX,NGRIDY,NGRIDZ)
      call fftK2X(NGRIDX,NGRIDY,NGRIDZ,NGRID1,NGRID2,pGrid)

      call ComputeStat(NGRIDX,NGRIDY,NGRIDZ,NGRID1,NGRID2,pGrid,ss1,ss2)
      xi0 = ss2**2*wGrid

      if(info .gt. 0) then
         write(6,*) '... xi(0): ', xi0
      endif

      fx = box(1)/NGRIDX
      fy = box(2)/NGRIDY
      fz = box(3)/NGRIDZ
      fVol = fx*fy*fz

      rmin = cell*0.5*sqrt(fx**2+fy**2+fz**2)
      rmax = cell*0.5*sqrt(box(1)**2+box(2)**2+box(3)**2)
      call SetXiFit(rmin,rmax)
C
C  First octant
C
      if(info .gt. 1) then
C$OMP PARALLEL DO DEFAULT(NONE),
C$OMP+PRIVATE(kc,jc,ic),
C$OMP+SHARED(pGrid)
         do kc=1,NGRIDZ
            do jc=1,NGRIDY
               do ic=1,NGRIDX
                  pGrid(ic,jc,kc) = 1.1e35
               enddo
            enddo
         enddo
      endif
C
C  Can this be done in parallel? It is not only complicated access
C  to pGrid, but also a call to xiFit...
C
      do kc=1,NGRIDZ/2+1
         do jc=1,kc
            do ic=1,jc
               r = sqrt(
     .              (fx*(ic-NGRIDX/2-1.0))**2+
     .              (fy*(jc-NGRIDY/2-1.0))**2+
     .              (fz*(kc-NGRIDZ/2-1.0))**2)
               if(r .lt. 1.0e-2) then
                  pGrid(ic,jc,kc) = xi0
               else                  
                  pGrid(ic,jc,kc) = fVol*xiFit(r*cell)
               endif
               pGrid(jc,ic,kc) = pGrid(ic,jc,kc)
               pGrid(jc,kc,ic) = pGrid(ic,jc,kc)
               pGrid(kc,jc,ic) = pGrid(ic,jc,kc)
               pGrid(kc,ic,jc) = pGrid(ic,jc,kc)
               pGrid(ic,kc,jc) = pGrid(ic,jc,kc)
            enddo
         enddo
      enddo
C
C  Other octants
C
C$OMP PARALLEL DO DEFAULT(NONE),
C$OMP+PRIVATE(kc,jc,ic),
C$OMP+SHARED(pGrid)
      do kc=1,NGRIDZ/2+1
         do jc=1,NGRIDY/2+1
            do ic=1,NGRIDX/2-1
               pGrid(NGRIDX+1-ic,jc,kc) = pGrid(ic+1,jc,kc)
            enddo
         enddo
         do jc=1,NGRIDY/2-1
            do ic=1,NGRIDX
               pGrid(ic,NGRIDY+1-jc,kc) = pGrid(ic,jc+1,kc)
            enddo
         enddo
      enddo
C
C  Other octants
C
C$OMP PARALLEL DO DEFAULT(NONE),
C$OMP+PRIVATE(kc,jc,ic),
C$OMP+SHARED(pGrid)
      do kc=1,NGRIDZ/2-1
         do jc=1,NGRIDY
            do ic=1,NGRIDX
               pGrid(ic,jc,NGRIDZ+1-kc) = pGrid(ic,jc,kc+1)
            enddo
         enddo
      enddo

      if(info .gt. 1) then
C$OMP PARALLEL DO DEFAULT(NONE),
C$OMP+PRIVATE(kc,jc,ic),
C$OMP+SHARED(pGrid)
         do kc=1,NGRIDZ
            do jc=1,NGRIDY
               do ic=1,NGRIDX
                  if(pGrid(ic,jc,kc) .gt. 1.0e35) then
                     write(0,*) 'ERROR: bug in ComputePGrid: ', 
     .                    ic, jc, kc
                     stop
                  endif
               enddo
            enddo
         enddo
      endif

      call InitFFT(NGRIDX,NGRIDY,NGRIDZ)
      call fftX2K(NGRIDX,NGRIDY,NGRIDZ,NGRID1,NGRID2,pGrid)

      return
      end
C
C
C
      subroutine ComputeStartEpoch(LEV)
      include 'cosmo.inc'
      include 'power.inc'
      include 'config.inc'

      rmsDel = sigmaL(cell*0.5**LEV)

      if(info .gt. 0) then
         write(6,*) 'Approximate rms overdensity: ', rmsDel
      endif

      aBegin = epsCell/rmsDel

      if(info .ge. 0) then
         write(6,*) 'Initial moment: ', aBegin
      endif

      return
      end
C
C
C
      subroutine SetDCMode(NLOW1,NLOW2,dLow)
      include 'dims.inc'
      include 'config.inc'
      dimension dLow(NLOW1,NLOW2,*)
      real*8 ss
C
C  Set the DC mode
C
      ss = 0.0d0
      do kc=1,NLOWZ
         do jc=1,NLOWY
            do ic=1,NLOWX
               ss = ss + dLow(ic,jc,kc)
            enddo
         enddo
      enddo
      ss = ss*sqrt(wLow)

      rmsDC = Ampk(0,0,0)/sqrt(wLow)
      delDC = rmsDC*real(ss)

      if(info .ge. 0) then
         write(6,*) 'Scaled DC mode: ', delDC, rmsDC
      endif

      if(info .gt. 1) then
         write(6,*) '... Delta_0: ',  rmsDC*Dplus(1.0), Dplus(1.0)
      endif

*      if(delDC .lt. -0.95 .or. delDC .gt. 1.5) then
*         write(0,*) 'ERROR: DCmode is way too large. Abort.'
*         stop
*      endif

      return
      end
C
C
C
      subroutine CheckStartEpoch(LEV,NX,NY,NZ,NDIM1,NDIM2,d,abeg,
     .     dSave,file,dirty)
      include 'cosmo.inc'
      include 'power.inc'
      include 'config.inc'
      dimension d(NDIM1,NDIM2,*)
      dimension dSave(NDIM1,NDIM2,*)
      character*(*) file
      logical dirty
C
      if(info .ge. 0) then
         write(6,*) 'Computing starting moment exactly...'
      endif

      call ComputeOverdensity(NX,NY,NZ,NDIM1,NDIM2,d,
     .     dSave,file,dirty)

      call ComputeStat(NX,NY,NZ,NDIM1,NDIM2,d,ss1,ss2)

      if(info .gt. 0) then
         write(6,*) '... rms overdensity: ', ss2
      endif

      abeg = epsCell/ss2

      if(info .ge. 0) then
         write(6,*) 'Exact initial moment: ', abeg, abeg/aBegin
      endif

      if(abs(abeg/aBegin-1.0) .gt. 0.2) then
         write(0,*) 'WARNING: error in aBegin is > 20%: ',
     .        aBegin, abeg
      endif

      return
      end
C
C
C
      subroutine ComputeOverdensity(NX,NY,NZ,NDIM1,NDIM2,d,
     .     dSave,file,dirty)
      include 'cosmo.inc'
      include 'power.inc'
      include 'config.inc'
      dimension d(NDIM1,NDIM2,*)
      dimension dSave(NDIM1,NDIM2,*)
      character*(*) file
      logical dirty
C
C  Restore original amplitudes
C
      call LoadAmplitudes(NX,NY,NZ,NDIM1,NDIM2,d,
     .     dSave,file,dirty)
C
C  The DC mode is included separately, so zero it explicitly
C
      d(1,1,1) = 0.0
      d(2,1,1) = 0.0

      call InitFFT(NX,NY,NZ)
      call fftK2X(NX,NY,NZ,NDIM1,NDIM2,d)
      dirty = .true.

      return
      end
C
C
C
      subroutine ComputeDisplacement(iDir,NX,NY,NZ,NDIM1,NDIM2,d,
     .     dSave,file,dirty)
      include 'cosmo.inc'
      include 'power.inc'
      include 'config.inc'
      dimension d(NDIM1,NDIM2,*)
      dimension dSave(NDIM1,NDIM2,*)
      character*(*) file
      logical dirty
C
      dimension ak(3)
C
      if(info .gt. 0) then
         write(6,*) '... direction: ', iDir
      endif
C
C  Restore original amplitudes
C
      call LoadAmplitudes(NX,NY,NZ,NDIM1,NDIM2,d,
     .     dSave,file,dirty)

C$OMP PARALLEL DO DEFAULT(NONE),
C$OMP+PRIVATE(kc,kkc,jc,jjc,ic,ak,ak2,icr,ici,udr,udi),
C$OMP+SHARED(akmin,NX,NY,NZ,d,iDir)
      do kc=1,NZ
         kkc = kc - 1
         if(kkc .ge. NZ/2) kkc = kkc - NZ
         ak(3) = akmin(3)*real(kkc)
         do jc=1,NY
            jjc = jc - 1
            if(jjc .ge. NY/2) jjc = jjc - NY
            ak(2) = akmin(2)*real(jjc)
            do ic=1,NX/2+1
               ak(1) = akmin(1)*real(ic-1)
               ak2 = ak(1)**2 + ak(2)**2 + ak(3)**2

               icr = 2*ic-1
               ici = 2*ic

               if(kc.eq.1 .and. jc.eq.1 .and. ic.eq.1) then
                  udr = 0.0
                  udi = 0.0
               else
                  udr = d(icr,jc,kc)/ak2
                  udi = d(ici,jc,kc)/ak2
               endif

               d(icr,jc,kc) = -ak(iDir)*udi
               d(ici,jc,kc) =  ak(iDir)*udr
      
            enddo
         enddo
      enddo

      call InitFFT(NX,NY,NZ)
      call fftK2X(NX,NY,NZ,NDIM1,NDIM2,d)
      dirty = .true.

      return
      end
C
C
C
      subroutine ComputeStat(NX,NY,NZ,NDIM1,NDIM2,d,ss1,ss2)
      dimension d(NDIM1,NDIM2,*)
C
      real*8 s1, s2
C
      s1 = 0.0d0
      s2 = 0.0d0
C$OMP PARALLEL DO DEFAULT(NONE),
C$OMP+PRIVATE(kc,jc,ic),
C$OMP+SHARED(d,NZ,NY,NX),
C$OMP+REDUCTION(+: s1,s2)
      do kc=1,NZ
         do jc=1,NY
            do ic=1,NX
               s1 = s1 + d(ic,jc,kc)
               s2 = s2 + d(ic,jc,kc)**2
            enddo
         enddo
      enddo
      vol = 1.0/real(NX)/real(NY)/real(NZ)
      s1 = s1*vol
      s2 = sqrt(s2*vol)

      ss1 = real(s1)
      ss2 = real(s2)

      return
      end
C
C
C
      subroutine ComputePS(NX,NY,NZ,NDIM1,NDIM2,d,file)
      include 'dims.inc'
      include 'power.inc'
      include 'config.inc'
      dimension d(NDIM1,NDIM2,*)
      character*(*) file
C
      parameter(N=601, almin=-3.0, alstp=0.01)
      real*8 ak(N), ps(N)
      dimension mp(N), pe(N), ak0(N)
C
C$OMP PARALLEL DO DEFAULT(NONE),
C$OMP+PRIVATE(i),
C$OMP+SHARED(ak0,ak,ps,mp)
      do i=1,N
         ak0(i) = 10.0**(almin+alstp*(i-1))
         ak(i) = 0.0d0
         ps(i) = 0.0d0
         mp(i) = 0
      enddo

      do kc=1,NZ
         kkc = kc - 1
         if(kkc .ge. NZ/2) kkc = kkc - NZ
         akz = akmin(3)*real(kkc)
         do jc=1,NY
            jjc = jc - 1
            if(jjc .ge. NY/2) jjc = jjc - NY
            aky = akmin(2)*real(jjc)
            do iic=0,NX/2
               akx = akmin(1)*real(iic)

               icr = 2*iic + 1
               ici = 2*iic + 2

               ak1 = sqrt(akx**2+aky**2+akz**2)/cell

               ik = 1 + nint((log10(1.0e-30+ak1)-almin)/alstp)
               if(ik.ge.1 .and. ik.le.N) then
                  ak(ik) = ak(ik) + dble(ak1)
                  ps(ik) = ps(ik) + dble(d(icr,jc,kc))**2 + 
     .                 dble(d(ici,jc,kc))**2
                  mp(ik) = mp(ik) + 1
               endif
   
            enddo
         enddo
      enddo

      fac = Dplus(1.0)**2

C$OMP PARALLEL DO DEFAULT(NONE),
C$OMP+PRIVATE(i),
C$OMP+SHARED(mp,ak,ak0,ps,pe,fac,cell3)
      do i=1,N
         if(mp(i) .gt. 0) then
            ak(i) = ak(i)/mp(i)
            ps(i) = fac*cell3*ps(i)/mp(i)/wLow
         else
            ak(i) = ak0(i)
            ps(i) = 0.0
         endif
         pe(i) = fac*Pk(ak0(i))
      enddo

      call OpenFile(11,file,IOWRITE)
      do i=1,N
         write(11,901) ak0(i), real(ps(i)), pe(i), real(ak(i))
      enddo
      close(11)

 901  format(4(e11.4,1x))

      return
      end
C
C
C
      subroutine ComputeSigma(R,NX,NY,NZ,NDIM1,NDIM2,d,s,
     .     dSave,file,dirty)
      include 'power.inc'
      include 'config.inc'
      dimension d(NDIM1,NDIM2,*)
      dimension dSave(NDIM1,NDIM2,*)
      character*(*) file
      logical dirty
C
C  Restore original amplitudes
C
      call LoadAmplitudes(NX,NY,NZ,NDIM1,NDIM2,d,
     .     dSave,file,dirty)
C
C$OMP PARALLEL DO DEFAULT(NONE),
C$OMP+PRIVATE(kc,kkc,jc,jjc,ic,akz,aky,akx,icr,ici,ak1,x,w),
C$OMP+SHARED(akmin,NZ,NY,NX,R,d)
      do kc=1,NZ
         kkc = kc - 1
         if(kkc .ge. NZ/2) kkc = kkc - NZ
         akz = akmin(3)*real(kkc)
         do jc=1,NY
            jjc = jc - 1
            if(jjc .ge. NY/2) jjc = jjc - NY
            aky = akmin(2)*real(jjc)
            do ic=1,NX/2+1
               akx = akmin(1)*real(ic-1)

               icr = 2*ic-1
               ici = 2*ic

               ak1 = sqrt(akx**2+aky**2+akz**2)

               x = ak1*R
               if(x .lt. 1.0e-3) then
                  w = 1.0 - x**2/10
               else
                  w = 3.0*(sin(x)-x*cos(x))/x**3
               endif

               d(icr,jc,kc) = w*d(icr,jc,kc)
               d(ici,jc,kc) = w*d(ici,jc,kc)

            enddo
         enddo
      enddo

      call InitFFT(NX,NY,NZ)
      call fftK2X(NX,NY,NZ,NDIM1,NDIM2,d)
      dirty = .true.

      call ComputeStat(NX,NY,NZ,NDIM1,NDIM2,d,ss1,ss2)
      s = ss2

      return
      end
C
C
C
      character*(*) function FileName(suffix)
      include 'config.inc'
      character*(*) suffix
C
      id = index(dir,' ') - 1
      il = index(label,' ') - 1
      ib = index(jobname,' ') - 1

      if(ib .gt. 1) then
         FileName = dir(1:id)//'/'//jobname(1:ib)//'_'//
     .        label(1:il)//'_'//kindNames(kindId)//'.'//suffix
      else
         FileName = dir(1:id)//'/'//'gic_'//
     .        label(1:il)//'_'//kindNames(kindId)//'.'//suffix
      endif

      return
      end
C
C
C
      subroutine OpenFile(id,suffix,mode)
      include 'config.inc'
      character*(*) suffix
C
      character*256 FileName
      external FileName
      character*256 jn
      logical mode
C
      parameter(iver = 100)
C
      if(id .ge. 20) then

         call ioOpenFile(id,FileName(suffix),mode)

         if(mode) then
            call ioWriteManifest(id,jobname,iver)
         else
            call ioReadManifest(id,jn,iv)
            if(iver.ne.iv .or. jobname.ne.jn) then
               write(0,*) 'ERROR: incompatible data file.'
               write(0,*) 'Jobname (code/file): ', jobname, jn
               write(0,*) 'Version (code/file): ', iver, iv
               stop
            endif
         endif

      else

         if(mode) then
            open(unit=id, file=FileName(suffix),
     .           form='formatted')
         else
            open(unit=id, file=FileName(suffix),
     .           form='formatted', status='old')
         endif
         
      endif

      return
      end
C
C
C
      subroutine OpenOutputFile(id,suffix,mode,NX,NY,NZ,nPart,idx)
      include 'cosmo.inc'
      include 'power.inc'
      include 'config.inc'
      logical mode
      character*(*) suffix
      integer*8 nPart, nPartin
C
      character*256 str, FileName
      external FileName
C
      call OpenFile(id,suffix,mode)
      if(mode) then
         call ioWriteFileHeader(id,OmB,OmX,OmL,OmN,h100,enn,sigma8,
     .        akpivot,aBegin,cell,delDC,rmsDC,NX,NY,NZ,iseed,iseed2,
     .        NRECORD,nPart,idx,0)
      else
         call ioReadFileHeader(id,OmBin,OmXin,OmLin,OmNin,h100in,ennin,
     .        sigma8in,akpivotin,aBeginin,cellin,delDCin,rmsDCin,
     .        NXin,NYin,NZin,iseedin,iseed2in,nrecin,nPartin,idx,iflag)

         if(iflag.ne.0 .or. 
     .        abs(OmB-OmBin).gt.1.0e-6 .or.
     .        abs(OmX-OmXin).gt.1.0e-6 .or.
     .        abs(OmL-OmLin).gt.1.0e-6 .or.
     .        abs(OmN-OmNin).gt.1.0e-6 .or.
     .        abs(h100-h100in).gt.1.0e-6 .or.
     .        abs(enn-ennin).gt.1.0e-6 .or.
     .        abs(sigma8-sigma8in).gt.1.0e-6 .or.
     .        abs(akpivot-akpivotin).gt.1.0e-6) then
            str = FileName(suffix)
            i = index(str,' ') - 1
            write(0,*) 'ERROR: incompatible data file ', str(1:i)
            stop
         endif

         if(aBegin .lt. 0.0) then
            aBegin = aBeginin
            cell = cellin
            delDC = delDCin
            rmsDC = rmsDCin
            write(6,*) 'aBegin = ', aBegin
            write(6,*) 'cell   = ', cell
            write(6,*) 'delDC  = ', delDC
            write(6,*) 'rmsDC  = ', rmsDC
         else
            if(iseed.ne.iseedin .or.
     .           abs(aBeginin/aBegin-1.0).gt.1.0e-5 .or. 
     .           abs(cellin/cell-1.0).gt.1.0e-5 .or.
     .           abs(rmsDCin-rmsDC).gt.1.0e-5 .or.
     .           abs(delDCin-delDC).gt.1.0e-5) then
               str = FileName(suffix)
               i = index(str,' ') - 1
               write(0,*) 'ERROR: incompatible data file ', str(1:i)
               stop
            endif
         endif

         if(
     .        (NXin.ne.NX .and. NX.ne.0) .or. 
     .        (NYin.ne.NY .and. NY.ne.0) .or.
     .        (NZin.ne.NZ .and. NZ.ne.0) .or. 
     .        (nPartin.ne.nPart .and. nPart.ne.0)) then
            str = FileName(suffix)
            i = index(str,' ') - 1
            write(0,*) 'ERROR: incompatible data file ', str(1:i)
            stop
         endif

         call OpenOutputFileDummy(NX,NY,NZ,nPart,NXin,NYin,NZin,nPartin)
         NRECORD = nrecin

      endif

      return
      end
C
C  The next one is to placate ftnchek
C
      subroutine OpenOutputFileDummy(NX,NY,NZ,nPart,n1,n2,n3,np)
      integer*8 nPart, np

      if(NX .eq. 0) NX = n1
      if(NY .eq. 0) NY = n2
      if(NZ .eq. 0) NZ = n3
      if(nPart .eq. 0) nPart = np

      return
      end
C
C
C
      subroutine WriteAmplitudes(suffix,NX,NY,NZ,NDIM1,NDIM2,d)
      include 'config.inc'
      dimension d(NDIM1,NDIM2,*)
      character*(*) suffix
 
      call OpenFile(99,suffix,IOWRITE)
      call WriteArray(99,NX+2,NY,NZ,NDIM1,NDIM2,d,1.0)
      call ioCloseFile(99)
      
      return
      end
C
C
C
      subroutine ReadAmplitudes(suffix,NX,NY,NZ,NDIM1,NDIM2,d)
      include 'config.inc'
      dimension d(NDIM1,NDIM2,*)
      character*(*) suffix
 
      call OpenFile(99,suffix,IOREAD)
      call ReadArray(99,NX+2,NY,NZ,NDIM1,NDIM2,d)
      call ioCloseFile(99)
      
      return
      end
C
C
C
      subroutine SaveAmplitudes(NX,NY,NZ,NDIM1,NDIM2,d,
     .     dSave,file,dirty)
      include 'config.inc'
      dimension d(NDIM1,NDIM2,*)
      dimension dSave(NDIM1,NDIM2,*)
      character*(*) file
      logical dirty

      if(dirty) then
         write(0,*) 'ERROR: saving dirty amplitudes.'
         stop
      endif

      if(useDisk) then
         call WriteAmplitudes(file,NX,NY,NZ,NDIM1,NDIM2,d)
      else
C$OMP PARALLEL DO DEFAULT(NONE),
C$OMP+PRIVATE(kc,jc,ic),
C$OMP+SHARED(d,dSave,NZ,NY,NX)
         do kc=1,NZ
            do jc=1,NY
               do ic=1,NX+2
                  dSave(ic,jc,kc) = d(ic,jc,kc)
               enddo
            enddo
         enddo
      endif

      return
      end
C
C
C
      subroutine LoadAmplitudes(NX,NY,NZ,NDIM1,NDIM2,d,
     .     dSave,file,dirty)
      include 'config.inc'
      dimension d(NDIM1,NDIM2,*)
      dimension dSave(NDIM1,NDIM2,*)
      character*(*) file
      logical dirty

      if(.not. dirty) return

      if(useDisk) then
         call ReadAmplitudes(file,NX,NY,NZ,NDIM1,NDIM2,d)
      else
C$OMP PARALLEL DO DEFAULT(NONE),
C$OMP+PRIVATE(kc,jc,ic),
C$OMP+SHARED(d,dSave,NZ,NY,NX)
         do kc=1,NZ
            do jc=1,NY
               do ic=1,NX+2
                  d(ic,jc,kc) = dSave(ic,jc,kc)
               enddo
            enddo
         enddo
      endif

      dirty = .false.

      return
      end
C
C
C
      subroutine WriteArray(id,NX,NY,NZ,NDIM1,NDIM2,d,fac)
      include 'config.inc'
      dimension d(NDIM1,NDIM2,*)
      dimension buffer(NRECORD)

      irec = 0
      do kc=1,NZ
         do jc=1,NY
            do ic=1,NX
               irec = irec + 1
               buffer(irec) = fac*d(ic,jc,kc)
               if(irec .eq. NRECORD) then
                  irec = 0
                  call ioWriteRealRecord(id,NRECORD,buffer)
               endif
            enddo
         enddo
      enddo
      
      if(irec .gt. 0) then
C
C  Pad with zeros to avoid garbage in the file
C
         do i=irec+1,NRECORD
            buffer(i) = 0.0
         enddo
         call ioWriteRealRecord(id,NRECORD,buffer)
      endif

      return
      end
C
C
C
      subroutine ReadArray(id,NX,NY,NZ,NDIM1,NDIM2,d)
      include 'config.inc'
      dimension d(NDIM1,NDIM2,*)
      dimension buffer(NRECORD)

      irec = NRECORD
      do kc=1,NZ
          do jc=1,NY
            do ic=1,NX
               if(irec .eq. NRECORD) then
                  irec = 0
                  call ioReadRealRecord(id,NRECORD,buffer)
               endif
               irec = irec + 1
               d(ic,jc,kc) = buffer(irec)
            enddo
         enddo
      enddo

      return
      end
C
C
C
      subroutine ReadArrayShifted(id,NX,NY,NZ,NDIM1,NDIM2,d,ish)
      include 'config.inc'
      dimension d(NDIM1,NDIM2,*), ish(3)
      dimension buffer(NRECORD)
C
C  Wrap around the shift values
C
      ish(1) = mod(mod(ish(1),NX)+NX,NX)
      ish(2) = mod(mod(ish(2),NY)+NY,NY)
      ish(3) = mod(mod(ish(3),NZ)+NZ,NZ)

      irec = NRECORD
      do kc=1,NZ
          do jc=1,NY
            do ic=1,NX
               if(irec .eq. NRECORD) then
                  irec = 0
                  call ioReadRealRecord(id,NRECORD,buffer)
               endif
               irec = irec + 1

               ic1 = 1 + mod(ic+ish(1)-1,NX)
               jc1 = 1 + mod(jc+ish(2)-1,NY)
               kc1 = 1 + mod(kc+ish(3)-1,NZ)

               d(ic1,jc1,kc1) = buffer(irec)
            enddo
         enddo
      enddo

      return
      end
C
C
C
      subroutine WriteMask(id,NX,NY,NZ,NDIM1,NDIM2,mask,ish)
      include 'config.inc'
      dimension mask(NDIM1,NDIM2,*), ish(3)
      dimension ibuffer(NRECORD)

      call ioWriteIntgRecord(id,3,ish)

      irec = 0
      do kc=1,NZ
         do jc=1,NY
            do ic=1,NX
               irec = irec + 1
               ibuffer(irec) = mask(ic,jc,kc)
               if(irec .eq. NRECORD) then
                  irec = 0
                  call ioWriteIntgRecord(id,NRECORD,ibuffer)
               endif
            enddo
         enddo
      enddo
      
      if(irec .gt. 0) then
C
C  Pad with zeros to avoid garbage in the file
C
         do i=irec+1,NRECORD
            ibuffer(i) = 0
         enddo
         call ioWriteIntgRecord(id,NRECORD,ibuffer)
      endif

      return
      end
C
C
C
      subroutine ReadMask(id,NX,NY,NZ,NDIM1,NDIM2,mask,ish)
      include 'config.inc'
      dimension mask(NDIM1,NDIM2,*), ish(3)
      dimension ibuffer(NRECORD)

      call ioReadIntgRecord(id,3,ish)

      irec = NRECORD
      do kc=1,NZ
          do jc=1,NY
            do ic=1,NX
               if(irec .eq. NRECORD) then
                  irec = 0
                  call ioReadIntgRecord(id,NRECORD,ibuffer)
               endif
               irec = irec + 1
               mask(ic,jc,kc) = ibuffer(irec)
            enddo
         enddo
      enddo

      return
      end
C
C
C
      subroutine WriteArrayLevel(id,NX,NY,NZ,NDIM1,NDIM2,d,fac,
     .     LEV,NLOW1,NLOW2,mask,lIndexed)
      include 'dims.inc'
      include 'config.inc'
      dimension d(NDIM1,NDIM2,*)
      dimension mask(NLOW1,NLOW2,*)
      logical lIndexed
      dimension buffer(NRECORD)
      dimension idx(NRECORD), jdx(NRECORD), kdx(NRECORD)

      NREF = 2**LEV
      if(
     .     NX.ne.NREF*NLOWX .or. 
     .     NY.ne.NREF*NLOWY .or.
     .     NZ.ne.NREF*NLOWZ) then
         write(0,*) 'ERROR: incompatible mask array: '
         write(0,*) NX, NY, NZ, NREF*NLOWX, NREF*NLOWY, NREF*NLOWZ
         stop
      endif

      irec = 0
      do kc0=1,NLOWZ
         do jc0=1,NLOWY
            do ic0=1,NLOWX
               if(mask(ic0,jc0,kc0) .eq. LEV) then
                  do kc1=1,NREF
                     kc = NREF*(kc0-1) + kc1
                     do jc1=1,NREF
                        jc = NREF*(jc0-1) + jc1
                        do ic1=1,NREF
                           ic = NREF*(ic0-1) + ic1
                           irec = irec + 1
                           buffer(irec) = fac*d(ic,jc,kc)
                           if(lIndexed) then
                              idx(irec) = ic
                              jdx(irec) = jc
                              kdx(irec) = kc
                           endif
                           if(irec .eq. NRECORD) then
                              irec = 0
                              call ioWriteRealRecord(id,NRECORD,buffer)
                              if(lIndexed) then
                                 call ioWriteIntgRecord(id,NRECORD,idx)
                                 call ioWriteIntgRecord(id,NRECORD,jdx)
                                 call ioWriteIntgRecord(id,NRECORD,kdx)
                              endif
                           endif
                        enddo
                     enddo
                  enddo
               endif
            enddo
         enddo
      enddo

      if(irec .gt. 0) then
C
C  Pad with zeros to avoid garbage in the file
C
         do i=irec+1,NRECORD
            buffer(i) = 0.0
            if(lIndexed) then
               idx(i) = 0
               jdx(i) = 0
               kdx(i) = 0
            endif
         enddo
         call ioWriteRealRecord(id,NRECORD,buffer)
         if(lIndexed) then
            call ioWriteIntgRecord(id,NRECORD,idx)
            call ioWriteIntgRecord(id,NRECORD,jdx)
            call ioWriteIntgRecord(id,NRECORD,kdx)
         endif
      endif

      return
      end
C
C
C
      subroutine ReadArrayLevelIndexed(id,NX,NY,NZ,NDIM1,NDIM2,d,
     .     lev,maxLev,nPart,lWithLevel,larr)
      include 'dims.inc'
      include 'config.inc'
      logical lWithLevel
      dimension d(NDIM1,NDIM2,*)
      dimension larr(NDIM1,NDIM2,*)
      integer*8 nPart, nRead
      dimension buffer(NRECORD)
      dimension idx(NRECORD), jdx(NRECORD), kdx(NRECORD)

      NLEV = 2**lev
      nxLev = NX*NLEV
      nyLev = NY*NLEV
      nzLev = NZ*NLEV

      NSUB = 2**(maxLev-lev)

      nRead = 0

      lmax = (nPart+NRECORD-1)/NRECORD
      do l=1,lmax

         call ioReadRealRecord(id,NRECORD,buffer)
         call ioReadIntgRecord(id,NRECORD,idx)
         call ioReadIntgRecord(id,NRECORD,jdx)
         call ioReadIntgRecord(id,NRECORD,kdx)

         if(nRead+NRECORD .gt. nPart) then
            imax = nPart - nRead
         else
            imax = NRECORD
         endif

         do i=1,imax
            if(
     .           idx(i).lt.1 .or. idx(i).gt.nxLev .or.
     .           jdx(i).lt.1 .or. jdx(i).gt.nyLev .or.
     .           kdx(i).lt.1 .or. kdx(i).gt.nzLev) then
               write(0,*) 'ERROR: corrupted indexed multi-res file: '
               write(0,*) l, i, idx(i), jdx(i), kdx(i), buffer(i)
               stop
            endif
            do kc1=1,NSUB
               kc = NSUB*(kdx(i)-1) + kc1
               do jc1=1,NSUB
                  jc = NSUB*(jdx(i)-1) + jc1
                  do ic1=1,NSUB
                     ic = NSUB*(idx(i)-1) + ic1
                     d(ic,jc,kc) = buffer(i)
                     if(lWithLevel) larr(ic,jc,kc) = lev
                  enddo
               enddo
            enddo
         enddo

         nRead = nRead + NRECORD

      enddo

      return
      end
C
C
C
      subroutine ReadArrayLevelSequential(id,nOff,d,
     .     lev,maxLev,nPart)
      include 'config.inc'
      dimension d(*)
      integer*8 nOff, nPart, nRead
      dimension buffer(NRECORD)

      nRead = 0

      lmax = (nPart+NRECORD-1)/NRECORD
      do l=1,lmax

         call ioReadRealRecord(id,NRECORD,buffer)

         if(nRead+NRECORD .gt. nPart) then
            imax = nPart - nRead
         else
            imax = NRECORD
         endif

         do i=1,imax
            d(nOff+nRead+i) = buffer(i)
         enddo

         nRead = nRead + NRECORD

      enddo

      return
      end
C
C
C
      subroutine InitFFT(NX,NY,NZ)
      common /InitFFT1/ NXprev, NYprev, NZprev
      data NXprev/0/, NYprev/0/, NZprev/0/
C
      if(NX.ne.NXprev .or. NY.ne.NYprev .or. NZ.ne.NZprev) then
         NXprev = NX
         NYprev = NY
         NZprev = NZ
         call fftInit(NX,NY,NZ)
      endif

      return
      end
C
C
C
      subroutine GetUnitFactors(dFac,xFac,vFac,wFac)
      include 'cosmo.inc'
      include 'config.inc'
      include 'cosmo_fun.inc'

      dFac = Dplus(aBegin)

      xFac = Dplus(aBegin)*cell
      
      vFac = Dplus(aBegin)*aBegin*H(aBegin)*100*cell
C
C  The value below quoted by Marcel.
C  http://ssd.jpl.nasa.gov/?constants
C      
      wFac = 2.775366272e11/h100*cell**3
      if(kindId .eq. 1) then
         wFac = wFac*OmM
      else if(kindId .eq. 2) then
         wFac = wFac*OmX
      else if(kindId .eq. 3) then
         wFac = wFac*OmB
      else
         write(0,*) 'ERROR: invalid kind Id: ', kindId
         stop
      endif

      return
      end

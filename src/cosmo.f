      subroutine InitCosmo
      include 'cosmo.inc'

      OmM = OmX + OmB
      OmR = 4.166e-5/h100**2
      OmK = 1 - OmL - OmM

      if(abs(OmK) .lt. 1.0e-6) then
         OmK = 0.0
         isLCDM = 1
      else
         isLCDM = 0
      endif

      return
      end
C
C
C
      function Dplus(aa)
      include 'cosmo.inc'

      if(isLCDM .eq. 1) then
         Dplus = DplusApp(aa)
      else
         Dplus = DplusGen(aa)
      endif

      return
      end
C
C
C
      function DplusApp(aa)
      include 'cosmo.inc'
      include 'cosmo_fun.inc'

      q = H2(aa)
           
      xM = OmM/(aa**3*q)
      xL = OmL/q

      DplusApp = 2.5*aa*xM/(xM**(4.0/7.0)-xL+(1+xM/2)*(1+xL/70))

      return
      end
C
C
C
      function DplusInt(aa)
      include 'cosmo.inc'
      real funDP
      external funDP
      include 'cosmo_fun.inc'

      a = aa
      call integ(funDP,0,1.0e-6,a,0.1,1.0e-3,s)

      DplusInt = 2.5*OmM*H(aa)*s

      return
      end
C
C
C
      function DplusGen(aa)
      include 'cosmo.inc'
C
      parameter (aini = 1.0e-5)
      parameter (errmax = 1.0e-4)
C
      include 'cosmo_fun.inc'
      adot(x) = x**3*H(x)

      a = aini
      b = a + 0.6666667*OmR/OmM
      c = adot(a)
 
      nstep = 0

      dt = 1.0
 
 10   nstep = nstep + 1
 
      a0 = a
      b0 = b
      c0 = c
 
 20   continue

      F0a = adot(a0)
      F0b = c0
      F0c = 1.5*OmM*a0*b0
 
      a1 = a0 + 2.0*dt*F0a/3.0
      b1 = b0 + 2.0*dt*F0b/3.0
      c1 = c0 + 2.0*dt*F0c/3.0
 
      F1a = adot(a1)
      F1b = c1
      F1c = 1.5*OmM*a1*b1 
 
      da0 = 0.75*F1a+0.25*F0a
      db0 = 0.75*F1b+0.25*F0b
      dc0 = 0.75*F1c+0.25*F0c
 
      a = a0 + dt*da0
      b = b0 + dt*db0
      c = c0 + dt*dc0

      err = 0.0

      d = a1 - a0
      if(abs(d) .gt. 0.0) err = max(err,4*(a-a0)**2/(9*d)/a1)
      d = b1 - b0
      if(abs(d) .gt. 0.0) err = max(err,4*(b-b0)**2/(9*d)/b1)
      d = c1 - c0
      if(abs(d) .gt. 0.0) err = max(err,4*(c-c0)**2/(9*d)/c1)
      err = err/errmax

      if(err .gt. 1.0) then
         if(dt .lt. 1.0e-10) then
            DPlusGen = 0.0
            return
         endif
         dt = 0.9*dt/(2+err)**0.5
         goto 20
      else
         dt = 0.9*dt/(0.01+err)**0.3
      endif
 
      if(a .ge. aa) then
         dt = (aa-a0)/da0
         DplusGen = b0 + dt*db0
         return
      endif
 
      if(nstep .lt. 999999) goto 10
 
      DplusGen = 0.0

      return
      end
C
C
C
      function funDP(x)
      include 'cosmo.inc'
      include 'cosmo_fun.inc'

      funDP = 1.0/(x**2*H2(x))**1.5

      return
      end
C
C  Comoving distance between a=1 and a=aa
C
      function XDist(aa)
      include 'cosmo.inc'
      real funXD
      external funXD
      include 'cosmo_fun.inc'

      a = aa
      b = 1.0
      call integ(funXD,0,a,b,0.1,1.0e-3,s)
      XDist = 2997.92*s

      return
      end
C
C
C
      function funXD(x)
      include 'cosmo.inc'
      include 'cosmo_fun.inc'

      funXD = 1.0/(x**2*H(x))

      return
      end

      subroutine nfft3is(n1,n2,n3,work,nw)
      real*4 work(nw,3)

      if(nw .lt. max(n1,2*n2,2*n3)+15) then
         write(0,*) 'Error in nfft3si: nw is too small. Abort. ',
     .        nw,max(n1,2*n2,2*n3)+15
         stop
      endif
      
      call rffti1(n1,work(1,1),work(n1+1,1))
      call cffti1(n2,work(1,2),work(2*n2+1,2))
      if(n3 .gt. 1) call cffti1(n3,work(1,3),work(2*n3+1,3))

      return
      end
C
C
C
      subroutine nfft3id(n1,n2,n3,work,nw)
      real*8 work(nw,3)

      if(nw .lt. max(n1,2*n2,2*n3)+15) then
         write(0,*) 'Error in nfft3si: nw is too small. Abort. ',
     .        nw,max(n1,2*n2,2*n3)+15
         stop
      endif
      
      call dffti1(n1,work(1,1),work(n1+1,1))
      call zffti1(n2,work(1,2),work(2*n2+1,2))
      if(n3 .gt. 1) call zffti1(n3,work(1,3),work(2*n3+1,3))

      return
      end
C
C
C
      subroutine nfft3sc(n1,n2,n3,af,no1,no2,work,nw)
      real*4 af(no1,no2,*), work(nw,3)
C
      parameter(NMAX=3100)
      real*4 z(2*NMAX,NMAX), w1(2*NMAX,NMAX)
C
      if(max(n1/2,n2,n3) .gt. NMAX) then
         write(0,*) 'Error in nfft3sc: NMAX is too small. Abort.',
     .        NMAX, n1/2, n2, n3
         stop
      endif

      if(nw .lt. max(n1,2*n2,2*n3)+15) then
         write(0,*) 'Error in nfft3sc: nw is too small. Abort. ',
     .        nw, max(n1,2*n2,2*n3)+15
         stop
      endif

C$OMP PARALLEL DO DEFAULT(NONE),
C$OMP+PRIVATE(k,j,i),
C$OMP+SHARED(n1,n2,n3,af,work,w1)
      do k=1,n3
         do j=1,n2

            call rfftf1(n1,af(1,j,k),w1(1,k),work(1,1),work(n1+1,1))

            af(n1+2,j,k) = 0.0
            do i=n1,2,-1
               af(i+1,j,k) = af(i,j,k)
            enddo
            af(2,j,k) = 0.0

         enddo
      enddo

C$OMP PARALLEL DO DEFAULT(NONE),
C$OMP+PRIVATE(k,j,i),
C$OMP+SHARED(n1,n2,n3,z,af,work,w1)
      do k=1,n3
         do i=1,n1/2+1

            do j=1,n2
               z(2*j-1,k) = af(2*i-1,j,k)
               z(2*j  ,k) = af(2*i  ,j,k)
            enddo

            call cfftf1(n2,z(1,k),w1(1,k),work(1,2),work(2*n2+1,2))

            do j=1,n2
               af(2*i-1,j,k) = z(2*j-1,k)
               af(2*i  ,j,k) = z(2*j  ,k)
            enddo

         enddo
      enddo

C$OMP PARALLEL DO DEFAULT(NONE),
C$OMP+PRIVATE(k,j,i),
C$OMP+SHARED(n1,n2,n3,z,af,work,w1)
      do j=1,n2
         do i=1,n1/2+1
               
            do k=1,n3
               z(2*k-1,j) = af(2*i-1,j,k)
               z(2*k  ,j) = af(2*i  ,j,k)
            enddo
      
            call cfftf1(n3,z(1,j),w1(1,j),work(1,3),work(2*n3+1,3))

            do k=1,n3
               af(2*i-1,j,k) = z(2*k-1,j)
               af(2*i  ,j,k) = z(2*k  ,j)
            enddo
      
         enddo
      enddo

      return
      end
C
C
C
      subroutine nfft3cs(n1,n2,n3,af,no1,no2,work,nw)
      real*4 af(no1,no2,*), work(nw,3)
C
      parameter(NMAX=3100)
      real*4 z(2*NMAX,NMAX), w1(2*NMAX,NMAX)
C
      if(max(n1/2,n2,n3) .gt. NMAX) then
         write(0,*) 'Error in nfft3sc: NMAX is too small. Abort.',
     .        NMAX, n1/2, n2, n3
         stop
      endif

      if(nw .lt. max(n1,2*n2,2*n3)+15) then
         write(0,*) 'Error in nfft3sc: nw is too small. Abort. ',
     .        nw, max(n1,2*n2,2*n3)+15
         stop
      endif

C$OMP PARALLEL DO DEFAULT(NONE),
C$OMP+PRIVATE(k,j,i),
C$OMP+SHARED(n1,n2,n3,z,af,work,w1)
      do j=1,n2
         do i=1,n1/2+1
               
            do k=1,n3
               z(2*k-1,j) = af(2*i-1,j,k)
               z(2*k  ,j) = af(2*i  ,j,k)
            enddo
      
            call cfftb1(n3,z(1,j),w1(1,j),work(1,3),work(2*n3+1,3))

            do k=1,n3
               af(2*i-1,j,k) = z(2*k-1,j)
               af(2*i  ,j,k) = z(2*k  ,j)
            enddo
      
         enddo
      enddo

C$OMP PARALLEL DO DEFAULT(NONE),
C$OMP+PRIVATE(k,j,i),
C$OMP+SHARED(n1,n2,n3,z,af,work,w1)
      do k=1,n3
         do i=1,n1/2+1
               
            do j=1,n2
               z(2*j-1,k) = af(2*i-1,j,k)
               z(2*j  ,k) = af(2*i  ,j,k)
            enddo
      
            call cfftb1(n2,z(1,k),w1(1,k),work(1,2),work(2*n2+1,2))

            do j=1,n2
               af(2*i-1,j,k) = z(2*j-1,k)
               af(2*i  ,j,k) = z(2*j  ,k)
            enddo
      
         enddo
      enddo

C$OMP PARALLEL DO DEFAULT(NONE),
C$OMP+PRIVATE(k,j,i),
C$OMP+SHARED(n1,n2,n3,af,work,w1)
      do k=1,n3
         do j=1,n2

            do i=2,n1
               af(i,j,k) = af(i+1,j,k)
            enddo

            call rfftb1(n1,af(1,j,k),w1(1,k),work(1,1),work(n1+1,1))

         enddo
      enddo

      return
      end
C
C
C
      subroutine nfft3dz(n1,n2,n3,af,no1,no2,work,nw)
      real*8 af(no1,no2,*), work(nw,3)
C
      parameter(NMAX=1024)
      real*8 z(2*NMAX,NMAX), w1(2*NMAX,NMAX)
C
      if(max(n1/2,n2,n3) .gt. NMAX) then
         write(0,*) 'Error in nfft3sc: NMAX is too small. Abort.',
     .        NMAX, n1/2, n2, n3
         stop
      endif

      if(nw .lt. max(n1,2*n2,2*n3)+15) then
         write(0,*) 'Error in nfft3sc: nw is too small. Abort. ',
     .        nw, max(n1,2*n2,2*n3)+15
         stop
      endif

C$OMP PARALLEL DO DEFAULT(NONE),
C$OMP+PRIVATE(k,j,i),
C$OMP+SHARED(n1,n2,n3,af,work,w1)
      do k=1,n3
         do j=1,n2

            call dfftf1(n1,af(1,j,k),w1(1,k),work(1,1),work(n1+1,1))

            af(n1+2,j,k) = 0.0d0
            do i=n1,2,-1
               af(i+1,j,k) = af(i,j,k)
            enddo
            af(2,j,k) = 0.0d0

         enddo
      enddo

C$OMP PARALLEL DO DEFAULT(NONE),
C$OMP+PRIVATE(k,j,i),
C$OMP+SHARED(n1,n2,n3,z,af,work,w1)
      do k=1,n3
         do i=1,n1/2+1

            do j=1,n2
               z(2*j-1,k) = af(2*i-1,j,k)
               z(2*j  ,k) = af(2*i  ,j,k)
            enddo

            call zfftf1(n2,z(1,k),w1(1,k),work(1,2),work(2*n2+1,2))

            do j=1,n2
               af(2*i-1,j,k) = z(2*j-1,k)
               af(2*i  ,j,k) = z(2*j  ,k)
            enddo

         enddo
      enddo

C$OMP PARALLEL DO DEFAULT(NONE),
C$OMP+PRIVATE(k,j,i),
C$OMP+SHARED(n1,n2,n3,z,af,work,w1)
      do j=1,n2
         do i=1,n1/2+1
               
            do k=1,n3
               z(2*k-1,j) = af(2*i-1,j,k)
               z(2*k  ,j) = af(2*i  ,j,k)
            enddo
      
            call zfftf1(n3,z(1,j),w1(1,j),work(1,3),work(2*n3+1,3))

            do k=1,n3
               af(2*i-1,j,k) = z(2*k-1,j)
               af(2*i  ,j,k) = z(2*k  ,j)
            enddo
      
         enddo
      enddo

      return
      end
C
C
C
      subroutine nfft3zd(n1,n2,n3,af,no1,no2,work,nw)
      real*8 af(no1,no2,*), work(nw,3)
C
      parameter(NMAX=1024)
      real*8 z(2*NMAX,NMAX), w1(2*NMAX,NMAX)
C
      if(max(n1/2,n2,n3) .gt. NMAX) then
         write(0,*) 'Error in nfft3sc: NMAX is too small. Abort.',
     .        NMAX, n1/2, n2, n3
         stop
      endif

      if(nw .lt. max(n1,2*n2,2*n3)+15) then
         write(0,*) 'Error in nfft3sc: nw is too small. Abort. ',
     .        nw, max(n1,2*n2,2*n3)+15
         stop
      endif

C$OMP PARALLEL DO DEFAULT(NONE),
C$OMP+PRIVATE(k,j,i),
C$OMP+SHARED(n1,n2,n3,z,af,work,w1)
      do j=1,n2
         do i=1,n1/2+1
               
            do k=1,n3
               z(2*k-1,j) = af(2*i-1,j,k)
               z(2*k  ,j) = af(2*i  ,j,k)
            enddo
      
            call zfftb1(n3,z(1,j),w1(1,j),work(1,3),work(2*n3+1,3))

            do k=1,n3
               af(2*i-1,j,k) = z(2*k-1,j)
               af(2*i  ,j,k) = z(2*k  ,j)
            enddo
      
         enddo
      enddo

C$OMP PARALLEL DO DEFAULT(NONE),
C$OMP+PRIVATE(k,j,i),
C$OMP+SHARED(n1,n2,n3,z,af,work,w1)
      do k=1,n3
         do i=1,n1/2+1
               
            do j=1,n2
               z(2*j-1,k) = af(2*i-1,j,k)
               z(2*j  ,k) = af(2*i  ,j,k)
            enddo
      
            call zfftb1(n2,z(1,k),w1(1,k),work(1,2),work(2*n2+1,2))

            do j=1,n2
               af(2*i-1,j,k) = z(2*j-1,k)
               af(2*i  ,j,k) = z(2*j  ,k)
            enddo
      
         enddo
      enddo

C$OMP PARALLEL DO DEFAULT(NONE),
C$OMP+PRIVATE(k,j,i),
C$OMP+SHARED(n1,n2,n3,af,work,w1)
      do k=1,n3
         do j=1,n2

            do i=2,n1
               af(i,j,k) = af(i+1,j,k)
            enddo

            call dfftb1(n1,af(1,j,k),w1(1,k),work(1,1),work(n1+1,1))

         enddo
      enddo

      return
      end

SRC = common.f config.f cosmo.f highres.f lowres.f power.f tools.f io.f fft.f nfft_pack.f
OBJ = $(patsubst %.f,.obj/%.o,$(SRC)) .obj/setup.o
LIB = .lib/fftpack.o .lib/gaussrand.o

SRC2 = $(patsubst %.f,src/%.f,$(SRC)) setup.f

ifdef PLATFORM
include ./platforms/$(PLATFORM)
.obj:
	mkdir .obj
else
.obj:
	@echo "A variable PLATFORM must be set. Available platforms:"
	@ls -m ./platforms
	@cat ./platforms >& /dev/null
endif

all: bin .obj .lib bin/gic-low bin/gic-high 

low-mem: bin .obj .lib bin/gic-high2

utils: bin .obj .lib bin/test bin/gic2ifrit bin/gic2lss bin/ascii2rfm bin/tipsy2rfm bin/make-mask bin/ascii2ifrit bin/read_manifest

bin:
	mkdir bin

.lib:
	mkdir .lib
	$(FC) -O2 -o .lib/fftpack.o -c src/fftpack.f
	$(FC) -O0 -o .lib/gaussrand.o -c src/gaussrand.f

bin/gic-low: $(OBJ) drivers/gic-low.f
	$(FC) $(FFLAGS) -c -o .obj/gic-low.o drivers/gic-low.f
	$(FC) $(FFLAGS) -o bin/gic-low .obj/gic-low.o $(OBJ) $(LIB)

bin/gic-high: $(OBJ) drivers/gic-high.F
	$(FC) $(FFLAGS) -c -o .obj/gic-high.o drivers/gic-high.F
	$(FC) $(FFLAGS) -o bin/gic-high .obj/gic-high.o $(OBJ) $(LIB)

bin/gic-high2: $(OBJ) drivers/gic-high.F
	$(FC) $(FFLAGS) -DLOW_MEMORY -c -o .obj/gic-high2.o drivers/gic-high.F
	$(FC) $(FFLAGS) -DLOW_MEMORY -o bin/gic-high2 .obj/gic-high2.o $(OBJ) $(LIB)

bin/test: $(OBJ) utils/test.f
	$(FC) $(FFLAGS) -Isrc -c -o .obj/test.o utils/test.f
	$(FC) $(FFLAGS) -o bin/test .obj/test.o $(OBJ) $(LIB)

bin/read_manifest: $(OBJ) utils/read_manifest.f
	$(FC) $(FFLAGS) -Isrc -c -o .obj/read_manifest.o utils/read_manifest.f
	$(FC) $(FFLAGS) -o bin/read_manifest .obj/read_manifest.o $(OBJ) $(LIB)

bin/gic2ifrit: $(OBJ) utils/gic2ifrit.f
	$(FC) $(FFLAGS) -Isrc -c -o .obj/gic2ifrit.o utils/gic2ifrit.f
	$(FC) $(FFLAGS) -o bin/gic2ifrit .obj/gic2ifrit.o $(OBJ) $(LIB)

bin/gic2lss: $(OBJ) utils/gic2lss.f
	$(FC) $(FFLAGS) -Isrc -c -o .obj/gic2lss.o utils/gic2lss.f
	$(FC) $(FFLAGS) -o bin/gic2lss .obj/gic2lss.o $(OBJ) $(LIB)

bin/ascii2ifrit: $(OBJ) utils/ascii2ifrit.f
	$(FC) $(FFLAGS) -Isrc -c -o .obj/ascii2ifrit.o utils/ascii2ifrit.f
	$(FC) $(FFLAGS) -o bin/ascii2ifrit .obj/ascii2ifrit.o $(OBJ) $(LIB)

bin/ascii2rfm: $(OBJ) utils/ascii2rfm.f
	$(FC) $(FFLAGS) -Isrc -c -o .obj/ascii2rfm.o utils/ascii2rfm.f
	$(FC) $(FFLAGS) -o bin/ascii2rfm .obj/ascii2rfm.o $(OBJ) $(LIB)

bin/tipsy2rfm: $(OBJ) utils/tipsy2rfm.f
	$(FC) $(FFLAGS) -Isrc -c -o .obj/tipsy2rfm.o utils/tipsy2rfm.f
	$(FC) $(FFLAGS) -o bin/tipsy2rfm .obj/tipsy2rfm.o $(OBJ) $(LIB)

bin/make-mask: $(OBJ) utils/make-mask.f
	$(FC) $(FFLAGS) -Isrc -c -o .obj/make-mask.o utils/make-mask.f
	$(FC) $(FFLAGS) -o bin/make-mask .obj/make-mask.o $(OBJ) $(LIB)

.obj/%.o: setup.inc src/dims.inc src/%.f
	$(FC) $(FFLAGS) -c -o $@ src/$(*F).f

.obj/setup.o: setup.inc setup.f
	$(FC) $(FFLAGS) -c -o $@ setup.f

check:
	make check-low check-high | more

check-low:
	ftnchek -notruncation -usage=1 drivers/gic-low.f $(SRC2) | fgrep -v "Including file "

check-high:
	cpp drivers/gic-high.F > /tmp/gic-high.f
	ftnchek -notruncation -usage=1 /tmp/gic-high.f $(SRC2) | fgrep -v "Including file "

check-test:
	ftnchek -notruncation -usage=1 -include=. utils/test.f $(SRC2) | fgrep -v "Including file "

check-gic2ifrit:
	ftnchek -notruncation -usage=1 -include=. utils/gic2ifrit.f $(SRC2) | fgrep -v "Including file "

check-tipsy2rfm:
	ftnchek -notruncation -usage=1 -include=. utils/tipsy2rfm.f $(SRC2) | fgrep -v "Including file "

check-make-mask:
	ftnchek -notruncation -usage=1 -include=. utils/make-mask.f $(SRC2) | fgrep -v "Including file "

clean:
	rm -rf .obj .lib bin core



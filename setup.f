      subroutine InitParameters
      include 'src/cosmo.inc'
      include 'src/power.inc'
      include 'src/config.inc'
      
      dir = 'OUT'
      jobname = 'rei20E'

      OmX = 0.2557
      OmB = 0.0479
      OmL = 0.6964
      OmN = 0
      h100 = 0.6814

      cell = 20.0/128
      enn = 0.9675
      sigma8 = 0.8285

      iseed = -5

      TFname = '/home/gnedin/IC/gic/TF/lingerics_lcdmP.lic'

      info = 1

      return
      end

      program gic
      include '../src/dims.inc'
      include '../src/config.inc'
C
      parameter(NOFF = 2)
      parameter(NLOW1 = NLOWX + 2 + NOFF)
      parameter(NLOW2 = NLOWY)
C
      common /save1/ dAmp(NLOW1,NLOW2,NLOWZ)
      common /save2/ dLow(NLOW1,NLOW2,NLOWZ)
      common /save3/ dSave(NLOW1,NLOW2,NLOWZ)
C
      logical dirty, lConfig(3)
C
      parameter(NOPTS = 7)
      character*99 opt(NOPTS)
      character*99 help(NOPTS)
      character*256 arg(NOPTS)

      data opt/'m$','d$','b$','no-pos$','no-vel$','no-den$',
     .     'save$'/
      data help/
     .     'create ICs for the total matter$',
     .     'create ICs for the dark matter$',
     .     'create ICs for the baryons$',
     .     'do not output positions$',
     .     'do not output velocities$',
     .     'do not output overdensities$',
     .     'save intermediate files for creating high-res ICs later$'/
C
C  Initialize
C
      call ReadCommandLine(0,NOPTS,3,arg,opt,help,' ')

      call Init
      useDisk = .false.

      call SetAmplitudes((arg(7) .ne. ' '),NLOW1,NLOW2,dAmp)
      call ComputeStartEpoch(0)

      lConfig(1) = (arg(4) .eq. ' ')
      lConfig(2) = (arg(5) .eq. ' ')
      lConfig(3) = (arg(6) .eq. ' ')
      do i=1,3
         if(arg(i) .eq. 'set') then
            call SetPk(i,aBegin)
            call CreateLowResIC(lConfig,NLOW1,NLOW2,dAmp,dLow,
     .           dSave,'tmp',dirty)
         endif
      enddo

      end


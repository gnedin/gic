      program gic2lss
      include 'dims.inc'
      include 'cosmo.inc'
      include 'config.inc'
C
      parameter(NOFF = 2)
      parameter(NLOW1 = NLOWX + 2 + NOFF)
      parameter(NLOW2 = NLOWY)
C
      common /saveL1/ d(NLOW1,NLOW2,NLOWZ)
C
      character*256 arg
      dimension ijk(3)
      integer*8 np
C
C  Initialize
C
      if(iargc() .lt. 1) then
         call getarg(0,arg)
         ib = index(arg,' ') - 1
         write(6,*) 'Usage: ', arg(1:ib), ' <file-root>'
         stop
      endif

      call Init

      call getarg(1,arg)
      call InterpretRootString(arg)
C
      label = 'lr'
      kindId = 1
      np = NLOWX*NLOWY*NLOWZ

      open(unit=24, file='stateIO.pm.restart', form='unformatted')
      write(24) NLOWX, NLOWY, NLOWZ
      write(24) OmX, OmB, OmL, h100, cell

      call OpenOutputFile(23,'pos',IOREAD,NLOWX,NLOWY,NLOWZ,np,0)
      write(24) aBegin
      do j=1,3
         call ReadArray(23,NLOWX,NLOWY,NLOWZ,NLOW1,NLOW2,d)

         do kc=1,NLOWZ
            do jc=1,NLOWY
               do ic=1,NLOWX
                  d(ic,jc,kc) = d(ic,jc,kc)/cell
               enddo
            enddo
         enddo

         call WriteLSSArray(24,NLOW1,NLOW2,d)
      enddo

      call ioCloseFile(23)

      vfac = 1.0e2*cell/aBegin
      call OpenOutputFile(23,'vel',IOREAD,NLOWX,NLOWY,NLOWZ,np,0)
      do j=1,3
         call ReadArray(23,NLOWX,NLOWY,NLOWZ,NLOW1,NLOW2,d)

         do kc=1,NLOWZ
            do jc=1,NLOWY
               do ic=1,NLOWX
                  d(ic,jc,kc) = d(ic,jc,kc)/vfac
               enddo
            enddo
         enddo

         call WriteLSSArray(24,NLOW1,NLOW2,d)
      enddo
      call ioCloseFile(23)

      close(24)

      return
      end
C
C
C
      subroutine WriteLSSArray(id,NLOW1,NLOW2,d)
      include 'dims.inc'
      include 'config.inc'
      dimension d(NLOW1,NLOW2,*)

      if(max(NLOWX,NLOWY,NLOWZ) .gt. 512) then
         do kc=1,NLOWZ
            write(id) ((d(ic,jc,kc),ic=1,NLOWX),jc=1,NLOWY)
         enddo
      else
         write(id) (((d(ic,jc,kc),ic=1,NLOWX),jc=1,NLOWY),kc=1,NLOWZ)
      endif

      return
      end

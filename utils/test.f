      program gic
      include 'config.inc'

      call Init

      write(6,*) 'D+(1) = ', Dplus(1.0)

*      call TestLowResDensity
*      call TestHighResDensity
*      call TestSigmaBox
*      call TestStartEpoch
*      call TestLowResDisplacements

      end
C
C
C
      subroutine TestLowResDisplacements
      include 'dims.inc'
      include 'config.inc'
C
      parameter(NOFF = 2)
      parameter(NLOW1 = NLOWX + 2 + NOFF)
      parameter(NLOW2 = NLOWY)
C
      common /saveL1/ d(NLOW1,NLOW2,NLOWZ)
C
      integer*8 np
      dimension ijk(3)

      label = 'lr'
      kindId = 1
      np = NLOWX*NLOWY*NLOWZ

      call OpenOutputFile(23,'pos',IOREAD,NLOWX,NLOWY,NLOWZ,np,0)
      do j=1,3
         call ReadArray(23,NLOWX,NLOWY,NLOWZ,NLOW1,NLOW2,d)
         do kc=1,NLOWZ
            ijk(3) = kc
            do jc=1,NLOWY
               ijk(2) = jc
               do ic=1,NLOWX
                  ijk(1) = ic
                  d(ic,jc,kc) = d(ic,jc,kc) - cell*(ijk(j)-0.5)
               enddo
            enddo
         enddo
         call ComputeStat(NLOWX,NLOWY,NLOWZ,NLOW1,NLOW2,d,ss1,ss2)
         write(6,*) 'Displacement[', j, '] (avg,rms): ', ss1, ss2
      enddo
      call ioCloseFile(23)

      call OpenOutputFile(23,'vel',IOREAD,NLOWX,NLOWY,NLOWZ,np,0)
      do j=1,3
         call ReadArray(23,NLOWX,NLOWY,NLOWZ,NLOW1,NLOW2,d)
         call ComputeStat(NLOWX,NLOWY,NLOWZ,NLOW1,NLOW2,d,ss1,ss2)
         write(6,*) 'Pec velocity[', j, '] (avg,rms): ', ss1, ss2
      enddo
      call ioCloseFile(23)

      return
      end
C
C
C
      subroutine TestLowResDensity
      include 'dims.inc'
      include 'config.inc'
C
      parameter(NOFF = 2)
      parameter(NLOW1 = NLOWX + 2 + NOFF)
      parameter(NLOW2 = NLOWY)
      parameter(NPART = NLOWX*NLOWY*NLOWZ)
C
      common /saveL1/ d(NLOW1,NLOW2,NLOWZ)
      common /saveL2/ x(3,NPART)
C
      label = 'lr'
      aBegin = -1.0
      call wTestDensity(NLOWX,NLOWY,NLOWZ,NLOW1,NLOW2,d,NPART,x)

      return
      end
C
C
C
      subroutine TestHighResDensity
      include 'dims.inc'
      include 'config.inc'
C
      parameter(NOFF = 2)
      parameter(NHIGH1 = NHIGHX + 2 + NOFF)
      parameter(NHIGH2 = NHIGHY)
      parameter(NPART = NHIGHX*NHIGHY*NHIGHZ)
C
      common /saveH1/ d(NHIGH1,NHIGH2,NHIGHZ)
      common /saveH2/ x(3,NPART)
C
      label = 'hr'//CHAR(48+LREF)
      aBegin = -1.0
      call wTestDensity(NHIGHX,NHIGHY,NHIGHZ,NHIGH1,NHIGH2,d,NPART,x)

      return
      end
C
C
C
      subroutine TestSigmaBox
      include 'dims.inc'
      include 'power.inc'
      include 'config.inc'
C
      parameter(NOFF = 2)
      parameter(NLOW1 = NLOWX + 2 + NOFF)
      parameter(NLOW2 = NLOWY)
C
      common /save0/ dAmp(NLOW1,NLOW2,NLOWZ)
      common /save1/ d(NLOW1,NLOW2,NLOWZ)
      common /save2/ dSave(NLOW1,NLOW2,NLOWZ)
C
      logical dirty, lConfig(3)

      info = -1

      ss1 = 0.0
      ss2 = 0.0
      nc = 0
      ds = 1.0

      aBegin = 1.0

      write(6,*) 'D+(z=0) = ', DPlus(1.0)

      lConfig(1) = .false.
      lConfig(2) = .false.
      lConfig(3) = .false.

      iseed = -1

      do while(nc.lt.3 .or. ds.gt.0.01) 
         do i=1,10
            iseed = iseed - 10
            call SetAmplitudes(.false.,NLOW1,NLOW2,dAmp)
            call CreateLowResIC(lConfig,NLOW1,NLOW2,dAmp,d,
     .           dSave,'tmp',dirty)
            call ComputeSigma(R8,NLOWX,NLOWY,NLOWZ,NLOW1,NLOW2,d,s,
     .           dSave,'tmp',dirty)
            s = s*Dplus(1.0)/sigma8
            ss1 = ss1 + s
            ss2 = ss2 + s**2
            nc = nc + 1
         enddo
         s1 = real(ss1)/nc
         s2 = sqrt(real(ss2)/nc-s1**2)
         ds = s2/(sqrt(real(nc))*s1)
         write(6,*) nc, s1, ds
      enddo
      write(6,*) cell*NLOW, s1, ds

      return
      end
C
C
C
      subroutine TestStartEpoch
      include 'dims.inc'
      include 'config.inc'
C
      parameter(NOFF = 2)
      parameter(NLOW1 = NLOWX + 2 + NOFF)
      parameter(NLOW2 = NLOWY)
C
      common /save0/ dAmp(NLOW1,NLOW2,NLOWZ)
      common /save1/ d(NLOW1,NLOW2,NLOWZ)
      common /save2/ dSave(NLOW1,NLOW2,NLOWZ)
C
      logical dirty, lConfig(3)

      info = -1

      ss1 = 0.0
      ss2 = 0.0
      nc = 0
      ds = 1.0

      lConfig(1) = .false.
      lConfig(2) = .false.
      lConfig(3) = .false.

      call SetPk(1,1.0)
      call ComputeStartEpoch(0)
      q1 = epsCell/aBegin

      do while(ds .gt. 1.0e-3) 
         do i=1,100
            iseed = -i
            call SetAmplitudes(.false.,NLOW1,NLOW2,dAmp)
            call CreateLowResIC(lConfig,NLOW1,NLOW2,dAmp,d,
     .           dSave,'tmp',dirty)
            call CheckStartEpoch(0,NLOWX,NLOWY,NLOWZ,NLOW1,NLOW2,d,abeg,
     .           dSave,'tmp',dirty)
            s = epsCell/abeg
            ss1 = ss1 + s
            ss2 = ss2 + s**2
            nc = nc + 1
         enddo
         s1 = real(ss1)/nc
         s2 = sqrt(real(ss2)/nc-s1**2)
         ds = s2/(nc*s1)
         write(6,*) nc, s1, ds
      enddo
      write(6,*) cell*NLOW, s1, ds

      write(6,*) s1, q1

      return
      end
C
C
C
      subroutine wTestDensity(NX,NY,NZ,NDIM1,NDIM2,d,NPART,x)
      include 'config.inc'
      dimension d(NDIM1,NDIM2,*), x(3,*)
      dimension ijk(3), dl(3), du(3)
      dimension dSave(1,1,1)
      integer*8 np
      character*3 str
      logical dirty
C
C  Initialize
C
      call InitFFT(NX,NY,NZ)
      vol = 1.0/real(NX)/real(NY)/real(NZ)

      useDisk = .true.

      call OpenOutputFile(21,'pos',IOREAD,NX,NY,NZ,np,0)
      do iDir=1,3
         call ReadArray(21,NX,NY,NZ,NDIM1,NDIM2,d)
         do kc=1,NZ
            do jc=1,NY
               do ic=1,NX
                  x(iDir,ic+NX*(jc-1+NZ*(kc-1))) = d(ic,jc,kc)
               enddo
            enddo
         enddo
      enddo
      call ioCloseFile(21)

      call OpenOutputFile(23,'den',IOREAD,NX,NY,NZ,np,0)
      call ReadArray(23,NX,NY,NZ,NDIM1,NDIM2,d)
      call ioCloseFile(23)

      fac = 1.0/Dplus(aBegin)
      do kc=1,NZ
         do jc=1,NY
            do ic=1,NX
               d(ic,jc,kc) = vol*d(ic,jc,kc)*fac
            enddo
         enddo
      enddo
      call fftX2K(NX,NY,NZ,NDIM1,NDIM2,d)
      call ComputePS(NX,NY,NZ,NDIM1,NDIM2,d,'psf')
      call fftK2X(NX,NY,NZ,NDIM1,NDIM2,d)

      call ComputeStat(NX,NY,NZ,NDIM1,NDIM2,d,ss1,ss2)
      write(6,*) 'File overdensity: ', ss1, ss2
  
      dirty = .false.
      call SaveAmplitudes(NX,NY,NZ,NDIM1,NDIM2,d,
     .     dSave,'tmp',dirty)

      do i=1,3
         R = 10**(0.5*i)
         call ComputeSigma(R,NX,NY,NZ,NDIM1,NDIM2,d,s,
     .        dSave,'tmp',dirty)
         write(6,*) 'File overdensity: ', R, s
      enddo
C
C  Do CIC
C
      do kc=1,NZ
         do jc=1,NY
            do ic=1,NX
               d(ic,jc,kc) = -1.0
            enddo
         enddo
      enddo

      do l=1,NPART

         do j=1,3
            x(j,l) = x(j,l)/cell

            if(x(j,l) .lt. 0.0) x(j,l) = x(j,l) + box(j)
            if(x(j,l) .ge. box(j)) x(j,l) = x(j,l) - box(j)
            ijk(j) = 1 + int(x(j,l))
            du(j) = x(j,l) - (ijk(j)-1)
            dl(j) = 1.0 - du(j)
         enddo

         ic0 = ijk(1)
         jc0 = ijk(2)
         kc0 = ijk(3)
         ic1 = ijk(1) + 1
         jc1 = ijk(2) + 1
         kc1 = ijk(3) + 1
         if(ic1 .gt. NX) ic1 = 1
         if(jc1 .gt. NY) jc1 = 1
         if(kc1 .gt. NZ) kc1 = 1

         d(ic0,jc0,kc0) = d(ic0,jc0,kc0) + dl(1)*dl(2)*dl(3)
         d(ic1,jc0,kc0) = d(ic1,jc0,kc0) + du(1)*dl(2)*dl(3)
         d(ic0,jc1,kc0) = d(ic0,jc1,kc0) + dl(1)*du(2)*dl(3)
         d(ic1,jc1,kc0) = d(ic1,jc1,kc0) + du(1)*du(2)*dl(3)
         d(ic0,jc0,kc1) = d(ic0,jc0,kc1) + dl(1)*dl(2)*du(3)
         d(ic1,jc0,kc1) = d(ic1,jc0,kc1) + du(1)*dl(2)*du(3)
         d(ic0,jc1,kc1) = d(ic0,jc1,kc1) + dl(1)*du(2)*du(3)
         d(ic1,jc1,kc1) = d(ic1,jc1,kc1) + du(1)*du(2)*du(3)
         
      enddo

      do kc=1,NZ
         do jc=1,NY
            do ic=1,NX
               d(ic,jc,kc) = vol*d(ic,jc,kc)*fac
            enddo
         enddo
      enddo
      call fftX2K(NX,NY,NZ,NDIM1,NDIM2,d)
      call ComputePS(NX,NY,NZ,NDIM1,NDIM2,d,'psg')
      call fftK2X(NX,NY,NZ,NDIM1,NDIM2,d)

      call ComputeStat(NX,NY,NZ,NDIM1,NDIM2,d,ss1,ss2)
      write(6,*) 'Grid overdensity: ', ss1, ss2
      write(6,*) 'Grid overdensity: ', delDC, sqrt(ss2**2+delDC**2)
      
      dirty = .false.
      call SaveAmplitudes(NX,NY,NZ,NDIM1,NDIM2,d,
     .     dSave,'tmp',dirty)

      do i=1,3
         R = 10**(0.5*i)
         call ComputeSigma(R,NX,NY,NZ,NDIM1,NDIM2,d,s,
     .        dSave,'tmp',dirty)
         write(6,*) 'Grid overdensity: ', R, sqrt(s**2+delDC**2)
      enddo

      return
      end


      program read_manifest
      character*256 str, jn

      if(iargc() .lt. 1) then
         write(0,*) 'Format: read_manifest <gic-file>'
         stop
      endif

      call getarg(1,str)

      call ioOpenFile(21,str,.false.)
      call ioReadManifest(21,jn,iv)
      
      write(6,*) 'File, Version=', iv
      ib = index(jn,' ') - 1
      write(6,*) '      Name=', jn(1:ib)

      call ioReadFileHeader(21,OmB,OmX,OmL,OmN,h100,enn,
     .     sigma8,akpivot,aBegin,cell,delDC,rmsDC,
     .     NXin,NYin,NZin,iseed,iseed2,nrec,nPart,idx,iflag)
      
      write(6,*) '      NX=', NXin
      write(6,*) '      NY=', NYin
      write(6,*) '      NZ=', NZin
      write(6,*) '      OmB=', OmB
      write(6,*) '      OmX=', OmX
      write(6,*) '      OmL=', OmL
      write(6,*) '      OmN=', OmN
      write(6,*) '      enn=', enn
      write(6,*) '      h100=', h100
      write(6,*) '      cell=', cell
      write(6,*) '      delDC=', delDC
      write(6,*) '      rmsDC=', rmsDC
      write(6,*) '      sigma8=', sigma8
      write(6,*) '      aBegin=', aBegin
      write(6,*) '      iseed1=', iseed
      write(6,*) '      iseed2=', iseed2

      call ioCloseFile(21)

      end



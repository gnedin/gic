      program ascii2ifrit
      include 'dims.inc'
      include 'config.inc'
C
      parameter(NLOW1 = NLOWX)
      parameter(NLOW2 = NLOWY)
C
      common /save1/ d(NLOW1,NLOW2,NLOWZ)
C
      dimension ish(3)
      character*256 arg
C
C  Initialize
C
      if(iargc() .lt. 1) then
         call getarg(0,arg)
         ib = index(arg,' ') - 1
         write(6,*) 'Usage: ', arg(1:ib), 
     .        ' <ascii-file> [ <ascii-file> ... ]'
         stop
      endif

      call Init

      do k=1,NLOWZ
         do j=1,NLOWY
            do i=1,NLOWX
               d(i,j,k) = 0.0
            enddo
         enddo
      enddo

      do im=1,iargc()

         call getarg(im,arg)
         i = index(arg,' ') - 1
         open(unit=99, file=arg(1:i), status='old', err=10)

         write(6,*) 'Reading file ', arg(1:i)
      
         read(99,*,err=20,end=20) isx, isy, isz
         if(im .eq. 1) then
            ish(1) = isx
            ish(2) = isy
            ish(3) = isz
         else if(
     .           isx.ne.ish(1) .or.
     .           isy.ne.ish(2) .or.
     .           isz.ne.ish(3)) then
            write(0,*) 'Incompatible shifts:'
            write(0,*) 'X: ', isx, ish(1)
            write(0,*) 'Y: ', isy, ish(2)
            write(0,*) 'Z: ', isz, ish(3)
            stop
         endif

         line = 0
 100     line = line + 1

         read(99,*,err=20,end=30) ix, iy, iz
         if(
     .        ix.lt.1 .or. ix.gt.NLOWX .or.
     .        iy.lt.1 .or. iy.gt.NLOWY .or.
     .        iz.lt.1 .or. iz.gt.NLOWZ) then
            write(0,*) 'Corrupted ascii mask file: '
            write(0,*) 'Line: ', line
            write(0,*) 'IX: ', ix, ' is outside the range 1 : ', NLOWX
            write(0,*) 'IY: ', iy, ' is outside the range 1 : ', NLOWY
            write(0,*) 'IZ: ', iz, ' is outside the range 1 : ', NLOWZ
            close(99)
            stop
         endif
      
         d(ix,iy,iz) = d(ix,iy,iz) + 2**(im-1)

         goto 100

 10      continue

         write(0,*) 'ERROR: mask file is not found.'
         stop

 20      continue

         write(0,*) 'ERROR: mask file is corrupted.'
         stop

 30      write(6,*) line, ' lines read.'
         close(99)

      enddo

      open(unit=41, file='ifrit-rfms.bin', form='unformatted')
      write(41) NLOWX, NLOWY, NLOWZ
      write(41) (((d(ic,jc,kc),ic=1,NLOWX),jc=1,NLOWY),kc=1,NLOWZ)
      close(41)
      
      end

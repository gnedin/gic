C
C Use this routine to set the mask of your choice
C
      subroutine MakeMask(NLOW1,NLOW2,mask,ish)
      include 'dims.inc'
      include 'config.inc'
      dimension mask(NLOW1,NLOW2,*), ish(3)

      xc = 0.5*NLOWX
      yc = 0.5*NLOWX
      zc = 0.5*NLOWX

      do i=1,3
         ish(i) = 0
      enddo

      do k=1,NLOWZ
         do j=1,NLOWY
            do i=1,NLOWX
               dx = (i-0.5) - xc
               dy = (j-0.5) - yc
               dz = (k-0.5) - zc

               if(dx .lt. -NLOWX/2) dx = dx + NLOWX
               if(dx .gt.  NLOWX/2) dx = dx - NLOWX
               if(dy .lt. -NLOWY/2) dy = dy + NLOWY
               if(dy .gt.  NLOWY/2) dy = dy - NLOWY
               if(dz .lt. -NLOWZ/2) dz = dz + NLOWZ
               if(dz .gt.  NLOWZ/2) dz = dz - NLOWZ

               r = sqrt(dx**2+dy**2+dz**2)
               d = max(abs(dx),abs(dy),abs(dz))

               if(d .lt. 16) then
                  mask(i,j,k) = 2
               else if(d .lt. 20) then
                  mask(i,j,k) = 1
               else
                  mask(i,j,k) = 0
               endif
            enddo
         enddo
      enddo

      return
      end
C
C
C
      program mmask
      include 'dims.inc'
      include 'config.inc'
C
      parameter(NLOW1 = NLOWX)
      parameter(NLOW2 = NLOWY)
C
      common /save1/ mask(NLOW1,NLOW2,NLOWZ), ish(3)
C
C  Initialize
C
      call Init

      call MakeMask(NLOW1,NLOW2,mask,ish)

      if(sublabel .ne. ' ') then
         isl = index(sublabel,' ') - 1
         label = 'lr'//sublabel(1:isl)
      else
         label = 'lr'
      endif

      kindId = 1
 
      call OpenFile(99,'rfm',IOWRITE)
      call WriteMask(99,NLOWX,NLOWY,NLOWZ,NLOW1,NLOW2,mask,ish)
      call ioCloseFile(99)
      
      end

      program tipsy2rfm
      include 'dims.inc'
      include 'config.inc'
C
      parameter(NLOW1 = NLOWX)
      parameter(NLOW2 = NLOWY)
      parameter(NPART = NLOWX*NLOWY*NLOWZ)
C
      common /save1/ mask(NLOW1,NLOW2,NLOWZ), ish(3)
C
      character*256 arg
C
C  Initialize
C
      if(iargc() .lt. 2) then
         call getarg(0,arg)
         ib = index(arg,' ') - 1
         write(6,*) 'Usage: ', arg(1:ib), 
     .        ' <file-root> <tipsy-file> [ extra-suffix ]'
         stop
      endif

      do i=1,3
         ish(i) = 0
      enddo

      call Init

      call getarg(1,arg)
      call InterpretRootString(arg)

      call getarg(2,arg)
      i = index(arg,' ') - 1
      open(unit=99, file=arg(1:i), status='old', err=10)
      read(99,*,err=20) n
      if(n .ne. NLOWX*NLOWY*NLOWZ) then
         write(0,*) 'Corrupted tipsy mask file: '
         write(0,*) 'File size: ', n
         write(0,*) 'Code size: ', NLOWX*NLOWY*NLOWZ
         close(99)
         stop
      endif
      do k=1,NLOWZ
         do j=1,NLOWY
            do i=1,NLOWX
               read(99,*,err=20) mask(i,j,k)
            enddo
         enddo
      enddo
      close(99)

      goto 30

 10   continue

      write(0,*) 'ERROR: mask file is not found.'
      stop

 20   continue

      write(0,*) 'ERROR: mask file is corrupted.'
      stop

 30   continue

      if(iargc() .gt. 2) then
         call getarg(3,sublabel)
         isl = index(sublabel,' ') - 1
         label = 'lr'//sublabel(1:isl)
      else
         label = 'lr'
      endif

      kindId = 1

      call OpenFile(99,'rfm',IOWRITE)
      call WriteMask(99,NLOWX,NLOWY,NLOWZ,NLOW1,NLOW2,mask,ish)
      call ioCloseFile(99)
      
      end

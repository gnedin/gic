      program ascii2rfm
      include 'dims.inc'
      include 'config.inc'
C
      parameter(NLOW1 = NLOWX)
      parameter(NLOW2 = NLOWY)
      parameter(NPART = NLOWX*NLOWY*NLOWZ)
C
      common /save1/ mask(NLOW1,NLOW2,NLOWZ), ish(3)
C
      character*256 arg
C
C  Initialize
C
      if(iargc() .lt. 1+LREF) then
         call getarg(0,arg)
         ib = index(arg,' ') - 1
         write(6,*) 'Usage: ', arg(1:ib), 
     .        ' <file-root> <ascii-file> ... [ extra-suffix ]'
         write(6,*) '(there should N ascii files for N-levels deep',
     .        ' mask).'
         stop
      endif

      call Init

      call getarg(1,arg)
      call InterpretRootString(arg)

      do k=1,NLOWZ
         do j=1,NLOWY
            do i=1,NLOWX
               mask(i,j,k) = 0
            enddo
         enddo
      enddo

      do l=1,LREF

         call getarg(1+l,arg)
         i = index(arg,' ') - 1
         open(unit=99, file=arg(1:i), status='old', err=10)

         read(99,*,err=20,end=20) isx, isy, isz
         if(l .eq. 1) then
            ish(1) = isx
            ish(2) = isy
            ish(3) = isz
         else if(
     .           isx.ne.ish(1) .or.
     .           isy.ne.ish(2) .or.
     .           isz.ne.ish(3)) then
            write(0,*) 'Incompatible shifts:'
            write(0,*) 'X: ', isx, ish(1)
            write(0,*) 'Y: ', isy, ish(2)
            write(0,*) 'Z: ', isz, ish(3)
            stop
         endif

         line = 0
 100     line = line + 1

         read(99,*,err=20,end=30) ix, iy, iz
         if(
     .        ix.lt.1 .or. ix.gt.NLOWX .or.
     .        iy.lt.1 .or. iy.gt.NLOWY .or.
     .        iz.lt.1 .or. iz.gt.NLOWZ) then
            write(0,*) 'Corrupted ascii mask file #', l, ':'
            write(0,*) 'Line: ', line
            write(0,*) 'IX: ', ix, ' is outside the range 1 : ', NLOWX
            write(0,*) 'IY: ', iy, ' is outside the range 1 : ', NLOWY
            write(0,*) 'IZ: ', iz, ' is outside the range 1 : ', NLOWZ
            close(99)
            stop
         endif
      
         mask(ix,iy,iz) = l

         goto 100

 10      continue

         write(0,*) 'ERROR: mask file #', l, ' is not found.'
         stop

 20      continue
         
         write(0,*) 'ERROR: mask file #', l, ' is corrupted.'
         stop

 30      write(6,*) 'Mask file #', l, ' done: ', line, ' lines read.'
         close(99)

      enddo

      if(iargc() .gt. 1+LREF) then
         call getarg(2+LREF,sublabel)
         isl = index(sublabel,' ') - 1
         label = 'lr'//sublabel(1:isl)
      else
         label = 'lr'
      endif

      kindId = 1
 
      call OpenFile(99,'rfm',IOWRITE)
      call WriteMask(99,NLOWX,NLOWY,NLOWZ,NLOW1,NLOW2,mask,ish)
      call ioCloseFile(99)
      
      end

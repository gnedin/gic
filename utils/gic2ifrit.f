      program gic2ifrit
      include 'dims.inc'
      include 'config.inc'
C
      parameter(NOFF = 2)
      parameter(NHIGH1 = NHIGHX + 2 + NOFF)
      parameter(NHIGH2 = NHIGHY)
      integer*8 NTMP, NPART
      parameter(NTMP = 1)
      parameter(NPART = NTMP*NHIGHX*NHIGHY*NHIGHZ)
C
      common /save1/ d(NHIGH1,NHIGH2,NHIGHZ)
      common /save2/ larr(NHIGH1,NHIGH2,NHIGHZ)
      common /save3/ z(NPART)
      dimension x(NPART), y(NPART)
      equivalence(x, d)
      equivalence(y, larr)
C
      character*256 str
C
      parameter(NOPTS = 3)
      character*99 opt(NOPTS)
      character*99 help(NOPTS)
      character*256 arg(NOPTS)

      data opt/'pos$','den$','d$='/
      data help/
     .     'create IFrIT particle file from GIC .pos file$',
     .     'create IFrIT mesh file from GIC .den file$',
     .     'set the output directory$'/
C
C  Initialize
C
      call ReadCommandLine(1,NOPTS,2,arg,opt,help,'<file-root>')

      call Init

      call getarg(1,str)
      call InterpretRootString(str)

      if(arg(3).ne.' ' .and. arg(3).ne.'set') then
         dir = arg(3)
      endif
C
C  Create particle file
C
      if(arg(1) .eq. 'set') then
         call OutputPositions(x,y,z)
      endif
C
C  Create mesh file
C
      if(arg(2) .eq. 'set') then
         call OutputOverdensity(NHIGH1,NHIGH2,d,larr)
      endif

      end
C
C
C
      subroutine OutputPositions(x,y,z)
      include 'dims.inc'
      include 'config.inc'
      dimension x(*)
      dimension y(*)
      dimension z(*)
C
      common /save0/ mask(NLOWX,NLOWY,NLOWZ), ish(3)
C
      parameter(MAXLEVS = 100)
      integer*8 noff, nptot, np, nplev(MAXLEVS)
      dimension buffer(NRECORD), ibuffer(NRECORD)
C
      character*256 FileName
      external FileName
C
      NX = 0
      NY = 0
      NZ = 0
      nptot = 0

      call OpenOutputFile(21,'pos',IOREAD,NX,NY,NZ,nptot,iflag)

      if(iflag .eq. 0) then
C
C  Simple uniform mesh
C
         if(NX.gt.NHIGHX .or. NY.gt.NHIGHY .or. NZ.gt.NHIGHZ) then
            write(0,*) 'ERROR: input file is too large;'
            write(0,*) 'Code dims: ', NX, NY, NZ
            write(0,*) 'File dims: ', NHIGHX, NHIGHY, NHIGHZ
            stop
         endif

         if(info .gt. 0) then
            write(6,*) '... reading dims: ', NX, NY, NZ, nptot
         endif

         call ReadArray(21,NX,NY,NZ,NX,NY,x)
         call ReadArray(21,NX,NY,NZ,NX,NY,y)
         call ReadArray(21,NX,NY,NZ,NX,NY,z)

      else
C
C  Multi-resolution mesh
C
         call ReadMask(21,NLOWX,NLOWY,NLOWZ,NLOWX,NLOWY,mask,ish)

         maxLev = iflag

         NMAX = 2**maxLev
         nxMax = NX*NMAX
         nyMax = NY*NMAX
         nzMax = NZ*NMAX

         if(nxMax.gt.NHIGHX .or. nyMax.gt.NHIGHY .or. 
     .        nzMax.gt.NHIGHZ) then
            write(0,*) 'ERROR: input file is too large;'
            write(0,*) 'File dims: ', nxMax, nxMax, nxMax
            write(0,*) 'Code dims: ', NHIGHX, NHIGHY, NHIGHZ
            stop
         endif

         noff = 0

         do lev=0,maxLev

            NLEV = 2**lev
            nxLev = NX*NLEV
            nyLev = NY*NLEV
            nzLev = NZ*NLEV

            call ioReadLevelHeader(21,ll,llmax,np,w,ind)
            if(ind.ne.0 .or. ll.ne.lev .or. llmax.ne.maxLev) then
               write(0,*) 'ERROR: corrupted input file;'
               write(0,*) 'Expected: ', lev, maxLev, 0
               write(0,*) 'Read in : ', ll, llmax, ind
               stop
            endif
               
            if(info .gt. 0) then
               write(6,*) '... reading level: ', lev, maxLev, np
            endif

            nplev(lev+1) = np

            call ReadArrayLevelSequential(21,noff,x,lev,maxLev,np)
            call ReadArrayLevelSequential(21,noff,y,lev,maxLev,np)
            call ReadArrayLevelSequential(21,noff,z,lev,maxLev,np)

            noff = noff + np

         enddo

         if(nptot .ne. noff) then
            write(0,*) 'ERROR: corrupted input file;'
            write(0,*) 'Expected number: ', nptot
            write(0,*) 'Actually read in: ', noff
            stop
         endif

      endif

      call ioCloseFile(21)

      open(unit=41, file=FileName('p.bin'), form='unformatted')
      write(41) int(nptot)
      write(41) 0.0, 0.0, 0.0, cell*box(1), cell*box(2), cell*box(3)
      write(41) (x(np), np=1,nptot)
      write(41) (y(np), np=1,nptot)
      write(41) (z(np), np=1,nptot)
C
C  Save level too
C
      if(iflag .gt. 0) then
         nlev = iflag + 1
         noff = 0
         do lev=1,nlev
            do np=1,nplev(lev)
               x(noff+np) = real(lev-1)
            enddo
            noff = noff + nplev(lev)
         enddo
         write(41) (x(np), np=1,nptot)
      endif

      close(41)

      return
      end
C
C
C
      subroutine OutputOverdensity(NHIGH1,NHIGH2,d,larr)
      include 'dims.inc'
      include 'config.inc'
      dimension d(NHIGH1,NHIGH2,*)
      dimension larr(NHIGH1,NHIGH2,*)
C
      common /save0/ mask(NLOWX,NLOWY,NLOWZ), ish(3)
C
      integer*8 np
      dimension buffer(NRECORD), ibuffer(NRECORD)
C
      character*256 FileName
      external FileName
C
      NX = 0
      NY = 0
      NZ = 0
      np = 0

      call OpenOutputFile(23,'den',IOREAD,NX,NY,NZ,np,iflag)

      if(iflag .eq. 0) then
C
C  Simple uniform mesh
C
         if(NX.gt.NHIGHX .or. NY.gt.NHIGHY .or. NZ.gt.NHIGHZ) then
            write(0,*) 'ERROR: input file is too large;'
            write(0,*) 'Code dims: ', NX, NY, NZ
            write(0,*) 'File dims: ', NHIGHX, NHIGHY, NHIGHZ
            stop
         endif

         if(info .gt. 0) then
            write(6,*) '... reading dims: ', NX, NY, NZ
         endif

         call ReadArray(23,NX,NY,NZ,NHIGH1,NHIGH2,d)

         do kc=1,NZ
            do jc=1,NY
               do ic=1,NX
                  larr(ic,jc,kc) = NREF
               enddo
            enddo
         enddo

      else
C
C  Multi-resolution mesh
C
         call ReadMask(23,NLOWX,NLOWY,NLOWZ,NLOWX,NLOWY,mask,ish)

         maxLev = iflag

         NMAX = 2**maxLev
         nxMax = NX*NMAX
         nyMax = NY*NMAX
         nzMax = NZ*NMAX

         if(nxMax.gt.NHIGHX .or. nyMax.gt.NHIGHY .or. 
     .        nzMax.gt.NHIGHZ) then
            write(0,*) 'ERROR: input file is too large;'
            write(0,*) 'File dims: ', nxMax, nxMax, nxMax
            write(0,*) 'Code dims: ', NHIGHX, NHIGHY, NHIGHZ
            stop
         endif

         if(info .gt. 1) then
            do kc=1,nzMax
               do jc=1,nyMax
                  do ic=1,nxMax
                     d(ic,jc,kc) = 1.1e35
                  enddo
               enddo
            enddo
         endif

         do lev=0,maxLev

            NLEV = 2**lev
            nxLev = NX*NLEV
            nyLev = NY*NLEV
            nzLev = NZ*NLEV

            call ioReadLevelHeader(23,ll,llmax,np,w,ind)
            if(ind.ne.1 .or. ll.ne.lev .or. llmax.ne.maxLev) then
               write(0,*) 'ERROR: corrupted input file;'
               write(0,*) 'Expected: ', lev, maxLev, 1
               write(0,*) 'Read in : ', ll, llmax, ind
               stop
            endif
               
            if(info .gt. 0) then
               write(6,*) '... reading level: ', lev, maxLev, np
            endif

            call ReadArrayLevelIndexed(23,NX,NY,NZ,NHIGH1,NHIGH2,d,
     .           lev,maxLev,np,.true.,larr)

         enddo

         if(info .gt. 1) then
            do kc=1,nzMax
               do jc=1,nyMax
                  do ic=1,nxMax
                     if(d(ic,jc,kc) .gt. 1.0e35) then
                        write(0,*) 'ERROR: unfilled cell: ', 
     .                       ic, jc, kc
                        stop
                     endif
                  enddo
               enddo
            enddo
         endif

         NX = nxMax
         NY = nyMax
         NZ = nzMax

      endif

      call ioCloseFile(23)

      open(unit=41, file=FileName('d.bin'), form='unformatted')
      write(41) NX, NY, NZ
      write(41) (((d(ic,jc,kc),ic=1,NX),jc=1,NY),kc=1,NZ)
      write(41) (((real(larr(ic,jc,kc)),ic=1,NX),jc=1,NY),kc=1,NZ)
      close(41)

      return
      end
